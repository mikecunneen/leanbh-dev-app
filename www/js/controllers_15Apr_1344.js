angular.module('starter.controllers', [ 'ngCookies', 'ngCordova', 'ngCordovaBluetoothLE', 'base64'])


.controller('MainCtrl', function($scope, $http) {



})



//anti tamper PIN Controller
.controller('UnlockController', function($scope, $location, $timeout, $state, $ionicModal) {


                $ionicModal.fromTemplateUrl('templates/pincodeModal.html', {
                scope: $scope
              }).then(function(modal) {
                $scope.modal = modal;
              });

                $scope.init = function() {

                   $scopeCorrectPasscode = 8585;
                   $scope.passcode = "";
                }

                $scope.add = function(value) {

                            if($scope.passcode.length < 4) {
                            $scope.passcode = $scope.passcode + value;
                            if($scope.passcode.length == 4) {
                                $timeout(function() {
                                    console.log("The four digit code was entered");
                                }, 500);

                                if($scope.passcode == $scopeCorrectPasscode)
                                {
                                console.log("Correct PIN :)");
                                //$state.go('tab.dash');

                                }



                            }
                        }


                }

                $scope.delete = function() {

                             if($scope.passcode.length > 0) {
                              $scope.passcode = $scope.passcode.substring(0, $scope.passcode.length - 1);
                          }

                }





})

//BLUETOOTH SIMPLE Stupid
.controller('btSSCtrl', function(networkService, chgServerApi, $base64, $state, $scope, $http, $timeout, $cordovaSQLite, $cordovaBluetoothLE)
{

      var thisBPData = [];
      var byteArrayCount = null;
      byteArrayCount = 0;

      var isIOS = ionic.Platform.isIOS();
      var isAndroid = ionic.Platform.isAndroid();

      var bpmUUID = null;

      $scope.result = {};
      //alert("****START of btSSCtrl---");

         //THIS block RELIES on the controller being loaded EVERY time a tplate loads... otherwise it wont work!!
         $scope.result.finalSys = window.localStorage['fSys'];
         $scope.result.finalDia = window.localStorage['fDia'];
         $scope.result.finalHr =  window.localStorage['fHr'];
         $scope.result.finalMap = window.localStorage['fMap'];




    //  if (isIOS == true)
      //{

      //  alert("isIOS=> " + isIOS);

                function closeConn(uuid)
                {

                           //clear these vars BUT BUT BUT Only after the conn is CLOSED and ALL Data is Got!
                           thisBPData = [];
                           byteArrayCount = 0;


                //  var  bpmUUID_ = "5E9B7376-173F-FE68-264F-38169673DFD8";

                          //alert("Closing bleConn... bpmUUID => " + uuid);
                          var params = {address: uuid};
                            console.log("close: " + JSON.stringify(params));




                                $cordovaBluetoothLE.close(params).then(function(obj) {
                                  console.log("Close Success : " + JSON.stringify(obj));
                                  }, function(obj) {
                                    console.log("Close Error : " + JSON.stringify(obj));
                                  });


                }


              //CROSS PLATFORM
              $scope.initBTConn = function()
              {



                        if (isIOS == true)
                        {

                                  $cordovaBluetoothLE.initialize({request:true}).then(null,
                                  function(obj) {
                                    //Handle errors
                                      console.log("Initialize Error : " + JSON.stringify(obj));

                                  },
                                  function(obj) {
                                        //success
                                              scanBle();
                                  }
                                );
                    }
                    else if (isAndroid == true)
                    {
                    //  alert("starting android BTSerial... >> calling  connectBTSerial()");

                       connectBTSerial();





                    }
                    else {
                      alert("Platform is Not iOS or Android. Are you using a Rotary phone???");
                    }

              }

                          function scanBle()
                          {

                                      $cordovaBluetoothLE.startScan({serviceUuids:[]}).then(null,
                                      function(obj)
                                      {
                                        //Handle errors
                                        console.log("BLE Scan Error: " +  JSON.stringify(obj));

                                      },
                                      function(obj)
                                      {
                                                  if (obj.status == "scanResult")
                                                  {
                                                    //Device found
                                                  console.log("******** DYNAMIC BLE Connection ******* Scan Result Present. obj.address: " + obj.address + " - full obj: " + JSON.stringify(obj) );
                                                    bpmUUID = obj.address;


                                                          connectBle(  bpmUUID  );


                                                                    //STOP SCAN
                                                                    function stopScan()
                                                                    {
                                                                          console.log("Stop Scan");

                                                                          $cordovaBluetoothLE.stopScan().then(function(obj) {
                                                                            console.log("Stop Scan Success : " + JSON.stringify(obj));
                                                                          }, function(obj) {
                                                                              console.log("Stop Scan Error : " + JSON.stringify(obj));
                                                                          });
                                                                    }

                                                                    stopScan();


                                                  }
                                                    else if (obj.status == "scanStarted")
                                                      {
                                                        //Scan started
                                                     console.log("Scan Started");
                                                      }
                                                    }
                                                  );

                              } //end scanBLE



                                          function connectBle(bpmUUID)
                                          {

                                            //alert("starting connect ble....");
                                              $cordovaBluetoothLE.connect({address: bpmUUID }).then(null, function(obj)
                                              {


                                                        //Handle errors
                                                  //  alert("Connect Error! Message: " + JSON.stringify(obj));


                                                                      if (obj.message == "Connection failed" )
                                                                      {

                                                                          //showBPData();
                                                                        //console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                                                        //  this.disconnect();
                                                                      alert("***** Connection Failed");
                                                                       closeConn(bpmUUID);


                                                                      // $state.go('tab.bpErrorOther');



                                                                        //  closeConn(bpmUUID);
                                                                        //  console.log("***** FINISHED CLOSING BT CONN");

                                                                          }







                                                    },
                                                    function(obj) {
                                                      if (obj.status == "connected")
                                                      {

                                                        //This works... :) //subscribe next...
                                                        console.log("connected. obj is: " + JSON.stringify(obj));

                                                        //may need to do a scan first as it gives a "service not found" error
                                                        //do a discover/ services (ios)... first..
                                                     console.log("call showServices() ");
                                                     showServices();

                                                      }
                                                      else if (obj.status == "connecting") {
                                                        //Device connecting
                                                        console.log("connecting....");

                                                      } else {

                                                          console.log("Conn. else... Should trigger on ios. window.localStorage['isAndroid'] is: " + window.localStorage['isAndroid']);



                                                                    console.log("disconnected...");

                                                                      if(thisBPData.length > 27)
                                                                      {
                                                                        alert("DISCONNECTED <<TRIGGER>> ; thisBPData.length: " + thisBPData.length);
                                                                        console.log("thisBPData[]: " + JSON.stringify(thisBPData));

                                                                           console.log("#### BPM RESULT************");

                                                                             var xSys = thisBPData.length - 8;
                                                                             var xDia = thisBPData.length - 7;
                                                                             var xHR = thisBPData.length - 6;

                                                                              var bpmSys = thisBPData[xSys];
                                                                              var bpmDia =  thisBPData[xDia];
                                                                              var bpmHR =  thisBPData[xHR];

                                                                             console.log("bpmSys: " + bpmSys);
                                                                             console.log("bpmDia: " + bpmDia);
                                                                             console.log("bpmHr: " + bpmHR); //had lc h which probably stopped nav to result...

                                                                            console.log("#### ///////BPM RESULT************");



                                                                      }
                                                                          //  console.log(" Calling showBPData().... just after disconnected....");
                                                                            //showBPData();
                                                                        //  console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                                                          //  this.disconnect();

                                                                 console.log("***** CLOSING BT CONN");
                                                                      closeConn(bpmUUID);
                                                                      console.log("***** FINISHED CLOSING BT CONN");

                                                                       console.log("***** GOING TO btConnClosed...");
                                                                    //   $state.go('tab.btConnClosed');

                                                                }






                                                      }
                                                    ); //end connect.then...




                                          } //end connect




                                    function showServices()
                                    {
                                      var params = {address: bpmUUID , serviceUuids: [ ] };
                                      $cordovaBluetoothLE.services(params).then(success, error);



                                                    function success(obj)
                                                    {
                                                //  alert("Services Success : " + JSON.stringify(obj));


                                                      if (obj.status == "services")
                                                      {
                                                        console.log("obj.status == Services");


                                                       var fff0Service =  obj.services[2];

                                                       getCharacteristics(fff0Service);


                                                      }
                                                      else
                                                      {
                                                        console.log("Unexpected Services Status");
                                                      }
                                                    }

                                                    function error(obj)
                                                    {
                                                        console.log("Services Error : " + JSON.stringify(obj));
                                                    }



                                    } //end this.showServices()



                                  function getCharacteristics(svc)
                                    {


                                    var params = {address: bpmUUID , service: svc};
                                    $cordovaBluetoothLE.characteristics(params).then(success, error);

                                                    function success(obj)
                                                    {
                                                  //   alert("Characteristics Success : " + JSON.stringify(obj));

                                                      if (obj.status == "characteristics")
                                                      {
                                                    //    alert("obj.status == characteristics");

                                                        var characteristics = obj.characteristics;

                                                              var fff1ch = 'FFF1';
                                                              var fff2ch = 'FFF2';

                                                          //    alert("About to subscribeBle... >> " + bpmUUID +  svc + fff1ch );
                                                              subscribeBle(bpmUUID, svc, fff1ch);



                                                      }
                                                      else
                                                      {
                                                        console.log("Unexpected Characteristics Status");
                                                      }
                                                    }

                                                    function error(obj)
                                                    {
                                                      console.log("Characteristics Error : " + JSON.stringify(obj));
                                                    }




                                    } //end getCHs




                                                function subscribeBle(uuid, svc, ch)
                                                {

                                                    console.log("***** START OF SubBLE......");

                                                    //window.localStorage['counter1'] = null;
                                                  //  window.localStorage['counter1'] = 0;

                                                    var params = {address:uuid, service:svc, characteristic:ch};
                                                    $cordovaBluetoothLE.subscribe(params).then(null, error, success);


                                                      //success == subscribed successfully;
                                                      function success(obj)
                                                      {

                                                        //console.log("SUB. SUCCESS :) [only once I think]"); //NOOOO.... every array!!!!!


                                                          //this runs for each array received from bpm
                                                          //c. 2 arrays per reading stored on device... >> so can be a LOT of arrays!
                                                          if (obj.status == "subscribedResult")
                                                          {

                                                          console.log("###RAW Base 64 String....");
                                                           console.log("#" + byteArrayCount + " : subscribedResult... Value is: " + JSON.stringify(obj.value)); //We get 194!!! subscribedResult
                                                           console.log("### //RAW Base 64 String....");

                                                                      byteArrayCount = byteArrayCount + 1;
                                                                      var bytes = $cordovaBluetoothLE.encodedStringToBytes(obj.value);


                                                                                    //  console.log("    ");
                                                                                  console.log("===== Start of Array # " + byteArrayCount + " =====");

                                                                                      var bytesLen = bytes.length;
                                                                                      for (var i = 0; i < bytesLen; i++)
                                                                                      {
                                                                                      console.log("bytes[" + i + "] is: " + bytes[i]);

                                                                                        //push each array element into a single array
                                                                                        thisBPData.push(bytes[i]);
                                                                                      }

                                                                                  //  console.log("after array... # " + byteArrayCount + ". thisBPData[]: " + JSON.stringify(thisBPData));


                                                                                  console.log("===== End of Array # " + byteArrayCount + " =====");
                                                                                    //  console.log("    ");

                                                                                          /*
                                                                                       //Save RAW Data to AppDB
                                                                                       var dt = new Date();
                                                                                       var utcDate = dt.toUTCString();

                                                                                          //create a rawDataDate var to be posted into the processedData tbl... >> to help link the raw and the processed data
                                                                                          window.localStorage['rawDataDate'] = utcDate;


                                                                                       var aUserID = window.localStorage['loggedIn_USERID'];
                                                                                       var aString = "No String Yet."
                                                                                       var aBpmID = "MicroLife BPM ID Num... - from CHG";
                                                                                       var aPhoneID = "An IMEI Num here... - from CHG";
                                                                                       var hex = "No HEX Today";


                                                                                                    //INSERTING THE RAW DATA TO APP DB
                                                                                                   function insertRawBPData(dateTime, uid, b64, str, hex, bpmid, phID, readingOutcome, readingOutcomeDetails ) {
                                                                                                          var query = "INSERT INTO bp_reading_raw (createdAt, userID, base64Data, stringData, hexData, bpmID, phoneID, readingOutcome, readingOutcomeDetails) VALUES (?,?,?,?,?,?,?,?,?)";
                                                                                                          $cordovaSQLite.execute(db, query, [dateTime, uid, b64, str, hex, bpmid, phID, readingOutcome, readingOutcomeDetails]).then(function(res) {
                                                                                                            console.log("INSERTED bpReadingRaw ID:" + res.insertId);
                                                                                                          }, function (err) {
                                                                                                            console.log(err);
                                                                                                          });
                                                                                                      }


                                                                                            */

                                                                //CHECK FOR BPM ERRORS!

                                                                //should only check first array for errors.
                                                                //eg 114 followed by 1 or 2 or 3 or 5...

                                                                if (byteArrayCount == 1 && bytes[12] == 114)
                                                                {
                                                              //  bpmErrorPresent = true;
                                                              //  console.log("BPM Error. Please Retake BP Reading.");
                                                                //  closeConn(uuid);


                                                                            bpmErrorPresent = true;
                                                                            console.log("BPM Error. Please Retake BP Reading.");
                                                                            console.log("The BPM Error Code is..... ");



                                                                                            if (  thisBPData[13] == 1)
                                                                                            {
                                                                                            //  alert("BPM Error 1");


                                                                                              readingOutcome = "BPM Error 1";
                                                                                              readingOutcomeDetails = "BPM Error 1: Signals too weak.";
                                                                                              bpmError1 = true;


                                                                                               console.log("inserting raw bp data to app db (bpm error 1)");
                                                                                              // insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                               console.log("inserting raw bp data DONE!");


                                                                                               $state.go('tab.bpError1');
                                                                                               bpmError1 = false;

                                                                                              closeConn(uuid);

                                                                                            }
                                                                                            else if ( thisBPData[13] == 2)
                                                                                            {
                                                                                            //  alert("BPM Error 2");


                                                                                              readingOutcome = "BPM Error 2";
                                                                                              readingOutcomeDetails = "BPM Error 2: Error Signal";
                                                                                              bpmError2 = true;


                                                                                               console.log("inserting raw bp data to app db (bpm error 2)");
                                                                                            //   insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                               console.log("inserting raw bp data DONE!");

                                                                                                 $state.go('tab.bpError2');
                                                                                                 bpmError2 = false;
                                                                                              closeConn(uuid);

                                                                                            }
                                                                                            else if ( thisBPData[13] == 3)
                                                                                            {
                                                                                      //  alert("BPM Error 3");

                                                                                            readingOutcome = "BPM Error 3";
                                                                                            readingOutcomeDetails = "BPM Error 3: No Pressure in the Cuff";
                                                                                            bpmError3 = true;


                                                                                             console.log("inserting raw bp data to app db (bpm error 3)");
                                                                                        //  insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                             console.log("inserting raw bp data DONE!");

                                                                                               $state.go('tab.bpError3');
                                                                                               bpmError3 = false;
                                                                                               closeConn(uuid);


                                                                                            }
                                                                                            else if (  thisBPData[13] == 5)
                                                                                            {
                                                                                            console.log("BPM Error 5");

                                                                                              readingOutcome = "BPM Error 5";
                                                                                              readingOutcomeDetails = "BPM Error 3: Abnormal Result";
                                                                                              bpmError5 = true;


                                                                                               console.log("inserting raw bp data to app db (bpm error 4)");
                                                                                            //   insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                               console.log("inserting raw bp data DONE!");

                                                                                                 $state.go('tab.bpError5');
                                                                                                 bpmError5 = false;
                                                                                                closeConn(uuid);

                                                                                            }
                                                                                            else if (  thisBPData[13] == 66)
                                                                                            {
                                                                                            console.log("BPM Error - Low Batteries");

                                                                                              readingOutcome = "BPM Error - Low Batteries";
                                                                                              readingOutcomeDetails = "BPM Error: Low Batteries";
                                                                                              bpmErrorLowBatt = true;


                                                                                               console.log("inserting raw bp data to app db (bpm error 4)");
                                                                                            //  insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                               console.log("inserting raw bp data DONE!");

                                                                                               $state.go('tab.bpmErrorLowBatt');
                                                                                               bpmError5 = false;
                                                                                              closeConn(uuid);

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                              console.log("Other BPM Error!");

                                                                                                 readingOutcome = "BPM Error - Other";
                                                                                                 readingOutcomeDetails = "BPM Error Other";
                                                                                                 bpmErrorOther = true;


                                                                                                 console.log("inserting raw bp data to app db (bpm error other)");
                                                                                            //     insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                 console.log("inserting raw bp data DONE!");

                                                                                                   $state.go('tab.bpErrorOther');
                                                                                                   bpmErrorOther = false;
                                                                                                   closeConn(uuid);


                                                                                            }






                                                                } //reason for: byteArrayCount <= 6  is to ensure we've added enuf data to thisBPData[] >> BUT <=2 would prob work... >> testing this...
                                                                //else if ( thisBPData[12] != 114 & thisBPData.length > 28 & byteArrayCount <= 6 )
                                                                else if ( thisBPData[12] != 114 & thisBPData.length > 28)
                                                                {
                                                                        console.log("# " + byteArrayCount +  " ; RESULT! + thisBPData.length > 28... ");
                                                                        //bpmErrorPresent = false;
                                                                      //  readingOutcome = "BP Reading Success";
                                                                      //  readingOutcomeDetails = "No BPM Errors Present. Reading Completed Successfully";
                                                                      //  insertRawBPData(utcDate, aUserID, obj.value, aString, convertedString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);

                                                                      //  closeConn(uuid);

                                                                       console.log("# " + byteArrayCount + ". thisBPData[]: " + JSON.stringify(thisBPData));
                                                                      //  console.log("@@@@@@@@# " + byteArrayCount + ". thisBPData[]: " + JSON.stringify(thisBPData));


                                                                            //    if (thisBPData.length >=29 && thisBPData.length < 30)
                                                                            //    {
                                                                                              //    console.log("^^^^^^^^^^^this.bpData.length < " + thisBPData.length  );


                                                                                                   var sys1 =  thisBPData[11];
                                                                                                   var dia1 = thisBPData[12];
                                                                                                   var hr1 = thisBPData[13];

                                                                                                    var sys2 =  thisBPData[18];
                                                                                                    var dia2 = thisBPData[19];
                                                                                                    var hr2 = thisBPData[20];

                                                                                                     var sys3 =  thisBPData[25];
                                                                                                     var dia3 = thisBPData[26];
                                                                                                     var hr3 = thisBPData[27];



                                                                                                     console.log("####Printing Intermediate Values...");
                                                                                                     console.log("sys1: " + sys1);
                                                                                                     console.log("dia1: " + dia1);
                                                                                                     console.log("hr1: " + hr1);

                                                                                                      console.log("sys2: " + sys2);
                                                                                                      console.log("dia2: " + dia2);
                                                                                                      console.log("hr2: " + hr2);

                                                                                                       console.log("sys3: " + sys3);
                                                                                                       console.log("dia3: " + dia3);
                                                                                                       console.log("hr3: " + hr3);


                                                                                                        console.log("#### ////// Printing Intermediate Values...");









                                                                                                   $scope.result.finalSys = (  sys1 + sys2 + sys3  ) / 3;
                                                                                                   $scope.result.finalDia = (  dia1 + dia2 + dia3  ) / 3;
                                                                                                   $scope.result.finalHr =  (  hr1 + hr2 + hr3 ) / 3;

                                                                                                   //MAP Calc. = [(2 x diastolic)+systolic] / 3
                                                                                                   $scope.result.finalMap = ( (2 * $scope.result.finalDia) + $scope.result.finalSys) / 3;
                                                                                                   //round to 2 dec. places
                                                                                                   $scope.result.finalMap = $scope.result.finalMap.toFixed(2);

                                                                                                   //set values to string + round them...
                                                                                                   $scope.result.finalSys = Math.round($scope.result.finalSys);
                                                                                                   $scope.result.finalDia = Math.round($scope.result.finalDia);
                                                                                                   $scope.result.finalHr = Math.round($scope.result.finalHr);





                                                                                                  console.log("");
                                                                                                  console.log("***** final results... ***");
                                                                                                  console.log(JSON.stringify($scope.result));
                                                                                                  console.log("***** ///final results... *** ,, NOW GOING to RESULT....");
                                                                                                  console.log("**** final array: thisBPData[]: " + JSON.stringify(thisBPData));



                                                                                                 //ie ONLY do this once
                                                                                                 //
                                                                                                 if (byteArrayCount == 3)
                                                                                                 {


                                                                                                                //Save Reading to AppDb - tbl: bp_reading_processed
                                                                                                               var dt = new Date();
                                                                                                               var utcDate = dt.toUTCString();
                                                                                                               var aUserID =  window.localStorage['loggedIn_USERID'];
                                                                                                               var aBpmID = 1;
                                                                                                               var aPhoneID =1;
                                                                                                               window.localStorage['rawDataDate'] = utcDate;



                                                                                                               function insertProcessedBPData(dateTime, rawDataDate,  uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, bpmid, phID )
                                                                                                               {

                                                                                                                      console.log("*****------- inside insertProcessedBPData");

                                                                                                                        db.transaction(function(tx) {
                                                                                                                                 var query = "INSERT INTO bp_reading_processed (createdAt, rawDataDate, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmID, phoneID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                                                                                                 tx.executeSql(query, [dateTime, rawDataDate, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmid, phID], function(tx, res) {
                                                                                                                                           console.log("INSERTED bpReadingProcessed. The ID is:" + res.insertId);
                                                                                                                                              window.localStorage['bprid'] = res.insertId;
                                                                                                                                               console.log("-- in insert proc. data. method window.localStorage['appDB_bprID'] == " + window.localStorage['bprid'] );

                                                                                                                                    }); //end outer tx >> insert into

                                                                                                                        },
                                                                                                                         function(err) {
                                                                                                                          alert('app db ERROR on Inserting BP Reading: ' + JSON.stringify(err));
                                                                                                                        });


                                                                                                                                      //Check to see that we have network conn.
                                                                                                                                       var networkConn = networkService.check();
                                                                                                                                        console.log("networkConn == " + networkConn);

                                                                                                                                          if (networkConn == 1)
                                                                                                                                          {
                                                                                                                                           console.log("**Insert to AppDB Done! Netw. Conn Present so calling postBPData()**");
                                                                                                                                            postBPData();

                                                                                                                                          }
                                                                                                                                          else {
                                                                                                                                           console.log("888888888««««««««««««««Insert to AppDB Done!  No Network Connection so Data Stored Locally Only!");
                                                                                                                                            // $state.go('tab.bpStep4');
                                                                                                                                             //bleService.closeConn();
                                                                                                                                          }

                                                                                                                  } //end insertProcessedBPData()



                                                                                                                        //this appears but insertProcessedBPData() appears not to work... >>> try and paste code into bel svc... itself.
                                                                                                                         console.log("sssssssssss-********   inserting bpReadingProcessed to app db.... + THEN http post to chg");
                                                                                                                           console.log("loggedIn_USERID => " + window.localStorage['loggedIn_USERID']);
                                                                                                                          var initChgSynched = 0;
                                                                                                                          insertProcessedBPData(utcDate,  window.localStorage['rawDataDate']  , aUserID, initChgSynched , sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, $scope.result.finalSys, $scope.result.finalDia, $scope.result.finalHr, $scope.result.finalMap, aBpmID, aPhoneID);
                                                                                                                         console.log("inserting bpReadingProcessed DONE!. Now Sending to CHG.....");









                                                                                     //THIS IS NOT BEING CALLED I THINK >> I think it is actually >
                                                                                     function postBPData()
                                                                                     {



                                                                                    console.log("*********Start of postBPData().. $http... etc"); //works but nothing else...


                                                                                                      var token = window.localStorage["TOKEN"];
                                                                                                      //var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IjE2MDEiLCJleHBpcmF0aW9uIjoxNDU5MzMxNTkyNjk3fQ.2OS3gyBGwHs8XjyEqaQs_wXR2eHXi97RnAGN3RwxLLQ";
                                                                                                      console.log("pdToken: " + token);
                                                                                                      var iOutcome = "Successful Reading";
                                                                                                      var iFeedback = "No Feedback";


                                                                                                      //http://leanbh.stephenlane.me:3128
                                                                                                      //chgServerApi.serverName + "/api/createBPReading"

                                                                                                      var fSys = $scope.result.finalSys;
                                                                                                      var fDia = $scope.result.finalDia;
                                                                                                      var fHr = $scope.result.finalHr;
                                                                                                      var fMap = $scope.result.finalMap;

                                                                                                      $http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReading", data: { token: token, bprCreatedAtRaw: window.localStorage['rawDataDate'], bprCreatedAt: utcDate, chgUserID: window.localStorage['loggedIn_USERNAME'], systolic: fSys, diastolic: fDia, hr: fHr, map: fMap, bprFeedback: iFeedback, bprOutcome: iOutcome, bpmID: aBpmID, phoneID: aPhoneID   } }).then(function successCallback(response)
                                                                                                        {

                                                                                                                  console.log("*******************Inside the $http post... response json: " + JSON.stringify(response));
                                                                                                                  console.log("token is: " + token);

                                                                                                                       var synchedFlag = 0;
                                                                                                                       synchedFlag = parseInt(response.data.synched);


                                                                                                                            console.log("synchedFlag == " + synchedFlag);


                                                                                                                    //  if (synchedFlag == 1)
                                                                                                                      // {


                                                                                                                                /*
                                                                                                                          var zbprid = parseInt(window.localStorage['bprid']);

                                                                                                                                      function updateBPR() {
                                                                                                                                                //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                                                                                                                                uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE rowid = ?";
                                                                                                                                                //alert("uQuery == " + uQuery);
                                                                                                                                             //window.sqlitePlugin.execute(db, uQuery, [1, zbprid ]).then(function(res) {

                                                                                                                                        console.log(" Row Updated Successfully! res JSON: " + JSON.stringify(res));


                                                                                                                                             }, function (err) {
                                                                                                                                               alert("update/synch error" + JSON.stringify(err));
                                                                                                                                             });
                                                                                                                                         }

                                                                                                                                        updateBPR();

                                                                                                                                        */
                                                                                                                            //  }




                                                                                                         }, function errorCallback(response) {
                                                                                                                      // called asynchronously if an error occurs
                                                                                                                      // or server returns response with an error status.


                                                                                                                         //alert("Error: " + response);
                                                                                                                         alert("Error Sending Data to CHG: " + JSON.stringify(response));
                                                                                                                          //console.log(response.data.message);



                                                                                                                    });

                                                                                         } //end postBPData







                                                                                                      window.localStorage['fSys'] = $scope.result.finalSys;
                                                                                                      window.localStorage['fDia'] = $scope.result.finalDia;
                                                                                                      window.localStorage['fHr'] = $scope.result.finalHr;
                                                                                                      window.localStorage['fMap'] = $scope.result.finalMap;

                                                                                                      $scope.result.finalSys = parseInt($scope.result.finalSys);
                                                                                                      $scope.result.finalDia = parseInt($scope.result.finalDia);

                                                                                                      console.log("#####  sys: " + $scope.result.finalSys + " ; dia: " + $scope.result.finalDia );


                                                                                                         //****ADD SYMPTOMS LATER!
                                                                                                         //check for high BP (MEDICAL Alerts)
                                                                                                         //$state.go('tab.bpStep4');
                                                                                                         //OK SYS: 90-159 // OK DIA: 40-99
                                                                                                         //HIGH SYS: >=160 // HIGH Dia: >=100
                                                                                                         //LOW SYS: <90 //LOW DIA: <40
                                                                                                         //if ($scope.result.finalSys > 90 & $scope.result.finalSys < 159 & $scope.result.finalDia > 40 & $scope.result.finalDia < 99 )
                                                                                                         //turn of medical alerts for now...
                                                                                                         if ($scope.result.finalSys != null )
                                                                                                         {
                                                                                                          //alert("3. BP is ok - NORMAL Result");
                                                                                                          //OLD values here...
                                                                                                          // $rootScope.$broadcast('eventFired', finalSys, finalDia, finalHr, finalMap);

                                                                                                         $state.go('tab.btSSResult');

                                                                                                         }
                                                                                                         else {
                                                                                                           console.log("$scope.result.finalSys is NULL >> NO RESULT");
                                                                                                         }




                                                                                                    /*     else if ($scope.result.finalSys >=160 || $scope.result.finalDia >= 100)
                                                                                                         {
                                                                                                        //  alert("3. Your BP is HIGH");
                                                                                                          $state.go('tab.bpWarning1');
                                                                                                        }
                                                                                                        else if ($scope.result.finalSys < 90 || $scope.result.finalDia < 40)
                                                                                                        {
                                                                                                        //  alert("3. Your BP is LOW");
                                                                                                          $state.go('tab.bpWarningLow');
                                                                                                        } */

                                                                                                      /*
                                                                                                        else if (symptomCount > 1)
                                                                                                        {
                                                                                                        //  alert("3. You've >1 Pregnancy Symptoms'");
                                                                                                          $state.go('tab.bpWarningLow');

                                                                                                        }
                                                                                                        else if ( symptomCount >= 1 & finalSys >= 140 & finalSys <= 159  ||  finalDia >=90  & finalDia <= 99  )
                                                                                                        {
                                                                                                         //alert("3. Borderline High BP WITH >= 1 Symptom");
                                                                                                         $state.go('tab.bpWarningCombined1');

                                                                                                        }
                                                                                                        */





                                                                                                 } //end  if (byteArrayCount == 3)








                                                                }  //end else if (ie no bpm errors [114 - 1,2,3,5 etc])







                                                            } //end if sub. result


                                                            else if (obj.status == "subscribed")
                                                            {
                                                              //use THIS to CONFIRM to user that you are connected!
                                                          console.log("Subscribed to BPM");

                                                            }
                                                            else
                                                            {
                                                            //  console.log("Unexpected Subscribe Status.");

                                                            }



                                                          } //end subscribeSuccess()


                                                          // subscribe Error present
                                                          function error(obj)
                                                          {
                                                          //  console.log("Subscribe Error : " + JSON.stringify(obj));
                                                          console.log("Subscribe Error : " + JSON.stringify(obj));

                                                          var iStr5 = "Subscribe Error : " + JSON.stringify(obj);
                                                          addToLog(iStr5, 16);

                                                          }


                                                    return false;






                                                } //end subscribeBle() ios






              //*********************************************************************************************************************

              //android specific bluetooth code... (BT Serial / SPP)
                  function enableBTSerial()
                  {

                                          bluetoothSerial.enable( function()
                                          {

                                          connectBTSerial();

                                          },  function()
                                              {
                                                  //  alert("The user did *not* enable Bluetooth");
                                              });

                  }

                  function isBTSerialEnabled()
                  {

                                            bluetoothSerial.isEnabled(
                                                      function() {
                                                      /// alert("BTSERIAL is enabled");




                                                                // listPairedDevices();


                                                      },
                                                      function() {
                                                          //alert("BTSERIAL is *not* enabled... calling enable bt serial");
                                                          enableBTSerial();
                                                      }
                                                  );




                    }


                    function disconnectBTSerial()
                    {

                         thisBPData = [];
                         byteArrayCount = 0;

                          //alert("about to disconnect...");
                          //THIS DOESNT wORK i thinkk..s
                          bluetoothSerial.disconnect([success], [failure]);


                    }





                  function connectBTSerial()
                  {



                                    isBTSerialEnabled();

                                      //this is SET in Platform.ready()
                                    //  alert("connecBTSerial: " + window.localStorage['mlAddr']);
                                      var addrx = window.localStorage['mlAddr'];
                                      bluetoothSerial.connect(addrx, connectSuccess, connectFailure);

                                                function connectSuccess()
                                                {


                                                //alert("Connected via BT Serial. Now Calling subcribe()");
                                                  subscribeBTSerial();
                                                }

                                                function connectFailure()
                                                {
                                                 //alert("Connection Failure!");

                                                    thisBPData = [];
                                                    byteArrayCount = 0;

                                                      if ( thisBPData.length > 0)
                                                      {

                                                      //  alert("normal disconnect!");
                                                        disconnectBTSerial();
                                                         //$state.go('tab.btConnClosed');
                                                            //use seperate service for that...
                                                          //  alert("Calling ....  parseBPDataService.parse()");
                                                          //  this.showBPData();
                                                          //parseBPDataService.parse();

                                                      }
                                                      else
                                                      {
                                                          //alert("Connection Failure + No Data from BPM. RESTARTING BP Reading!");
                                                        //  $state.go('tab.bpErrorOther');
                                                      //  alert("***** GOING TO btConnClosed...");
                                                      //   $state.go('tab.btConnClosed');

                                                          disconnectBTSerial();
                                                      }


                                                }







                    } //end connectBTSerial




                                function subscribeBTSerial()
                                {
                                   //empty  thisBPData = []; WHEN Subscribe is called!!
                                  // thisBPData = [];
                                //   alert(" start of subscribeBTSerial thisBPData = ?? " +  JSON.stringify(thisBPData));

                                    //alert("start of this.subscribeBTSerial... ");
                                  //  var byteArrayCount = null;
                                  //  byteArrayCount = 0;

                                    //var counter1 = 0;
                                    window.localStorage['counter1'] = null;
                                    window.localStorage['counter1'] = 0;


                                                            bluetoothSerial.subscribeRawData(onRawData, subscribeFailed);

                                                                // this runs for every byteArray.... could run 5-* TIMES > depending on data in bpm
                                                                function onRawData(data)
                                                                {

                                                                //alert("raw data received!!! data: " + JSON.stringify(data));

                                                                    //this is a counter to see which array we are on... there will usually be many arrays and we are most interested in the first one
                                                                    byteArrayCount = byteArrayCount + 1;
                                                                    //alert("Just incremented byteArrayCount. It's now = " + byteArrayCount);



                                                                      var rawData = new Uint8Array(data);
                                                                      //alert("rawData (json) is: " + JSON.stringify(rawData) );
                                                                        var dataArrayJSON = JSON.stringify(rawData);


                                                                        for (var e = 0; e < rawData.length; e++)
                                                                        {
                                                                        thisBPData.push(rawData[e]);
                                                                        }



                                                                                                 //Save RAW Data to AppDB >> this is v similar to code in the ble section... >> insert this value to b64 field: dataArrayJSON

                                                                                                 var dt = new Date();
                                                                                                 var utcDate = dt.toUTCString();

                                                                                                 //create a rawDataDate var to be posted into the processedData tbl... >> to help link the raw and the processed data
                                                                                                 window.localStorage['rawDataDate'] = utcDate;


                                                                                                 var aUserID = window.localStorage['loggedIn_USERID'];
                                                                                                 var aString = "No String Yet.";
                                                                                                 var aBpmID = window.localStorage['mlAddr'];
                                                                                                 var aPhoneID = "An IMEI Num here... - from CHG";


                                                                                                              //INSERTING THE RAW DATA TO APP DB
                                                                                                          /*   function insertRawBPData(dateTime, uid, b64, str, hex, bpmid, phID, readingOutcome, readingOutcomeDetails ) {
                                                                                                            //   alert("starting insertRawBPData()....");
                                                                                                                    var query = "INSERT INTO bp_reading_raw (createdAt, userID, base64Data, stringData, hexData, bpmID, phoneID, readingOutcome, readingOutcomeDetails) VALUES (?,?,?,?,?,?,?,?,?)";
                                                                                                                    $cordovaSQLite.execute(db, query, [dateTime, uid, b64, str, hex, bpmid, phID, readingOutcome, readingOutcomeDetails]).then(function(res) {
                                                                                                                    //alert("INSERTED bpReadingRaw ID:" + res.insertId);
                                                                                                                    }, function (err) {
                                                                                                                    alert(err);
                                                                                                                    });
                                                                                                                } */




                                                                                                        //CHECK FOR BPM ERRORS!
                                                                                                        if ( byteArrayCount == 1 && thisBPData[12] == 114 )
                                                                                                        {
                                                                                                          alert("thisBPData[12]=>114; 13=> " + thisBPData[13]);
                                                                                                        bpmErrorPresent = true;
                                                                                                        console.log("BPM Error. Please Retake BP Reading.");
                                                                                                        console.log("The BPM Error Code is..... ");

                                                                                                                        if (  thisBPData[13] == 1)
                                                                                                                        {
                                                                                                                        //  alert("BPM Error 1");


                                                                                                                          readingOutcome = "BPM Error 1";
                                                                                                                          readingOutcomeDetails = "BPM Error 1: Signals too weak.";
                                                                                                                          bpmError1 = true;


                                                                                                                           console.log("inserting raw bp data to app db (bpm error 1)");
                                                                                                                          // insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                                           console.log("inserting raw bp data DONE!");


                                                                                                                           $state.go('tab.bpError1');
                                                                                                                           bpmError1 = false;

                                                                                                                           disconnectBTSerial();

                                                                                                                        }
                                                                                                                        else if ( thisBPData[13] == 2)
                                                                                                                        {
                                                                                                                        //  alert("BPM Error 2");


                                                                                                                          readingOutcome = "BPM Error 2";
                                                                                                                          readingOutcomeDetails = "BPM Error 2: Error Signal";
                                                                                                                          bpmError2 = true;


                                                                                                                           console.log("inserting raw bp data to app db (bpm error 2)");
                                                                                                                        //   insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                                           console.log("inserting raw bp data DONE!");

                                                                                                                             $state.go('tab.bpError2');
                                                                                                                             bpmError2 = false;
                                                                                                                             disconnectBTSerial();

                                                                                                                        }
                                                                                                                        else if ( thisBPData[13] == 3)
                                                                                                                        {
                                                                                                                  //  alert("BPM Error 3");

                                                                                                                        readingOutcome = "BPM Error 3";
                                                                                                                        readingOutcomeDetails = "BPM Error 3: No Pressure in the Cuff";
                                                                                                                        bpmError3 = true;


                                                                                                                         console.log("inserting raw bp data to app db (bpm error 3)");
                                                                                                                    //  insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                                         console.log("inserting raw bp data DONE!");

                                                                                                                           $state.go('tab.bpError3');
                                                                                                                           bpmError3 = false;
                                                                                                                           disconnectBTSerial();


                                                                                                                        }
                                                                                                                        else if (  thisBPData[13] == 5)
                                                                                                                        {
                                                                                                                        console.log("BPM Error 5");

                                                                                                                          readingOutcome = "BPM Error 5";
                                                                                                                          readingOutcomeDetails = "BPM Error 3: Abnormal Result";
                                                                                                                          bpmError5 = true;


                                                                                                                           console.log("inserting raw bp data to app db (bpm error 4)");
                                                                                                                        //   insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                                           console.log("inserting raw bp data DONE!");

                                                                                                                             $state.go('tab.bpError5');
                                                                                                                             bpmError5 = false;
                                                                                                                             disconnectBTSerial();

                                                                                                                        }
                                                                                                                        else if (  thisBPData[13] == 66)
                                                                                                                        {
                                                                                                                        console.log("BPM Error - Low Batteries");

                                                                                                                          readingOutcome = "BPM Error - Low Batteries";
                                                                                                                          readingOutcomeDetails = "BPM Error: Low Batteries";
                                                                                                                          bpmErrorLowBatt = true;


                                                                                                                           console.log("inserting raw bp data to app db (bpm error 4)");
                                                                                                                        //  insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                                           console.log("inserting raw bp data DONE!");

                                                                                                                           $state.go('tab.bpmErrorLowBatt');
                                                                                                                           bpmError5 = false;
                                                                                                                           disconnectBTSerial();

                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                          console.log("Other BPM Error!");

                                                                                                                             readingOutcome = "BPM Error - Other";
                                                                                                                             readingOutcomeDetails = "BPM Error Other";
                                                                                                                             bpmErrorOther = true;


                                                                                                                             console.log("inserting raw bp data to app db (bpm error other)");
                                                                                                                        //     insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                                             console.log("inserting raw bp data DONE!");

                                                                                                                               $state.go('tab.bpErrorOther');
                                                                                                                               bpmErrorOther = false;
                                                                                                                               disconnectBTSerial();


                                                                                                                        }


                                                                                                        } //end if there's an error
                                                                                                          else if ( thisBPData[12] != 114 & thisBPData.length > 28 & byteArrayCount <= 6 )
                                                                                                          {

                                                                                                            //  alert("no errors!!!");
                                                                                                                      //  alert("# " + byteArrayCount +  " ; RESULT! + thisBPData.length > 28... ");
                                                                                                                            //bpmErrorPresent = false;
                                                                                                                          //  readingOutcome = "BP Reading Success";
                                                                                                                          //  readingOutcomeDetails = "No BPM Errors Present. Reading Completed Successfully";
                                                                                                                          //  insertRawBPData(utcDate, aUserID, obj.value, aString, convertedString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);

                                                                                                                          //  closeConn(uuid);

                                                                                                                          //  alert("# " + byteArrayCount + ". thisBPData[]: " + JSON.stringify(thisBPData));
                                                                                                                          //  console.log("@@@@@@@@# " + byteArrayCount + ". thisBPData[]: " + JSON.stringify(thisBPData));


                                                                                                                                //    if (thisBPData.length >=29 && thisBPData.length < 30)
                                                                                                                                //    {
                                                                                                                                                  //    console.log("^^^^^^^^^^^this.bpData.length < " + thisBPData.length  );


                                                                                                                                                       var sys1 =  thisBPData[11];
                                                                                                                                                       var dia1 = thisBPData[12];
                                                                                                                                                       var hr1 = thisBPData[13];

                                                                                                                                                        var sys2 =  thisBPData[18];
                                                                                                                                                        var dia2 = thisBPData[19];
                                                                                                                                                        var hr2 = thisBPData[20];

                                                                                                                                                         var sys3 =  thisBPData[25];
                                                                                                                                                         var dia3 = thisBPData[26];
                                                                                                                                                         var hr3 = thisBPData[27];

                                                                                                                                                         //CLEAR THE ARRAY TO STOP ISSUES ON RESTARTS....
                                                                                                                                                          //thisBPData = [];



                                                                                                                                                       $scope.result.finalSys = (  sys1 + sys2 + sys3  ) / 3;
                                                                                                                                                       $scope.result.finalDia = (  dia1 + dia2 + dia3  ) / 3;
                                                                                                                                                       $scope.result.finalHr =  (  hr1 + hr2 + hr3 ) / 3;

                                                                                                                                                       //MAP Calc. = [(2 x diastolic)+systolic] / 3
                                                                                                                                                       $scope.result.finalMap = ( (2 * $scope.result.finalDia) + $scope.result.finalSys) / 3;
                                                                                                                                                       //round to 2 dec. places
                                                                                                                                                       $scope.result.finalMap = $scope.result.finalMap.toFixed(2);

                                                                                                                                                       //set values to string + round them...
                                                                                                                                                       $scope.result.finalSys = Math.round($scope.result.finalSys);
                                                                                                                                                       $scope.result.finalDia = Math.round($scope.result.finalDia);
                                                                                                                                                       $scope.result.finalHr = Math.round($scope.result.finalHr);





                                                                                                                                                      console.log("");
                                                                                                                                                      console.log("***** final results... ***");
                                                                                                                                                      console.log(JSON.stringify($scope.result));
                                                                                                                                                      console.log("***** ///final results... *** ,, NOW GOING to RESULT....");



                                                                                                                                                     //ie ONLY do this once
                                                                                                                                                     //
                                                                                                                                                     if (byteArrayCount == 3)
                                                                                                                                                     {

                                                                                                                                                    //  alert("---*******************byteArrayCount => " + byteArrayCount + "  (should be 3!)");


                                                                                                                                                                    //Save Reading to AppDb - tbl: bp_reading_processed
                                                                                                                                                                   var dt = new Date();
                                                                                                                                                                   var utcDate = dt.toUTCString();
                                                                                                                                                                   var aUserID =  window.localStorage['loggedIn_USERID'];
                                                                                                                                                                   var aBpmID = 1;
                                                                                                                                                                   var aPhoneID =1;
                                                                                                                                                                   window.localStorage['rawDataDate'] = utcDate;



                                                                                                                                                                   function insertProcessedBPData(dateTime, rawDataDate,  uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, bpmid, phID )
                                                                                                                                                                   {

                                                                                                                                                                      //  alert("*****------- inside insertProcessedBPData");

                                                                                                                                                                            db.transaction(function(tx) {
                                                                                                                                                                                     var query = "INSERT INTO bp_reading_processed (createdAt, rawDataDate, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmID, phoneID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                                                                                                                                                     tx.executeSql(query, [dateTime, rawDataDate, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmid, phID], function(tx, res) {
                                                                                                                                                                                               console.log("INSERTED bpReadingProcessed. The ID is:" + res.insertId);
                                                                                                                                                                                                  window.localStorage['bprid'] = res.insertId;
                                                                                                                                                                                                   console.log("-- in insert proc. data. method window.localStorage['appDB_bprID'] == " + window.localStorage['bprid'] );

                                                                                                                                                                                        }); //end outer tx >> insert into

                                                                                                                                                                            },
                                                                                                                                                                             function(err) {
                                                                                                                                                                              alert('app db ERROR on Inserting BP Reading: ' + JSON.stringify(err));
                                                                                                                                                                            });


                                                                                                                                                                                          //Check to see that we have network conn.
                                                                                                                                                                                           var networkConn = networkService.check();
                                                                                                                                                                                            console.log("networkConn == " + networkConn);

                                                                                                                                                                                              if (networkConn == 1)
                                                                                                                                                                                              {
                                                                                                                                                                                               console.log("**Insert to AppDB Done! Netw. Conn Present so calling postBPData()**");
                                                                                                                                                                                                postBPData();

                                                                                                                                                                                              }
                                                                                                                                                                                              else {
                                                                                                                                                                                               console.log("888888888««««««««««««««Insert to AppDB Done!  No Network Connection so Data Stored Locally Only!");
                                                                                                                                                                                                // $state.go('tab.bpStep4');
                                                                                                                                                                                                 //bleService.closeConn();
                                                                                                                                                                                              }

                                                                                                                                                                      } //end insertProcessedBPData()



                                                                                                                                                                            //this appears but insertProcessedBPData() appears not to work... >>> try and paste code into bel svc... itself.
                                                                                                                                                                             console.log("sssssssssss-********   inserting bpReadingProcessed to app db.... + THEN http post to chg");
                                                                                                                                                                               console.log("loggedIn_USERID => " + window.localStorage['loggedIn_USERID']);
                                                                                                                                                                              var initChgSynched = 0;
                                                                                                                                                                              insertProcessedBPData(utcDate,  window.localStorage['rawDataDate']  , aUserID, initChgSynched , sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, $scope.result.finalSys, $scope.result.finalDia, $scope.result.finalHr, $scope.result.finalMap, aBpmID, aPhoneID);
                                                                                                                                                                             console.log("inserting bpReadingProcessed DONE!. Now Sending to CHG.....");









                                                                                                                                         //THIS IS NOT BEING CALLED I THINK >> I think it is actually >
                                                                                                                                         function postBPData()
                                                                                                                                         {



                                                                                                                                      //  alert("*********Start of postBPData().. $http... etc"); //works but nothing else...


                                                                                                                                                          var token = window.localStorage["TOKEN"];
                                                                                                                                                          //var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IjE2MDEiLCJleHBpcmF0aW9uIjoxNDU5MzMxNTkyNjk3fQ.2OS3gyBGwHs8XjyEqaQs_wXR2eHXi97RnAGN3RwxLLQ";
                                                                                                                                                          console.log("pdToken: " + token);
                                                                                                                                                          var iOutcome = "Successful Reading";
                                                                                                                                                          var iFeedback = "No Feedback";


                                                                                                                                                          //http://leanbh.stephenlane.me:3128
                                                                                                                                                          //chgServerApi.serverName + "/api/createBPReading"

                                                                                                                                                          var fSys = $scope.result.finalSys;
                                                                                                                                                          var fDia = $scope.result.finalDia;
                                                                                                                                                          var fHr = $scope.result.finalHr;
                                                                                                                                                          var fMap = $scope.result.finalMap;

                                                                                                                                                                        //  alert("about to postBPData... window.localStorage['loggedIn_USERID'] is (should be chgUserID and NOT Username): " + window.localStorage['loggedIn_USERID'] );
                                                                                                                                                                          //setting this to window.localStorage['loggedIn_USERID'], for now... as it will match ios...
                                                                                                                                                          $http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReading", data: { token: token, bprCreatedAtRaw: window.localStorage['rawDataDate'], bprCreatedAt: utcDate, chgUserID: window.localStorage['loggedIn_USERID'], systolic: fSys, diastolic: fDia, hr: fHr, map: fMap, bprFeedback: iFeedback, bprOutcome: iOutcome, bpmID: aBpmID, phoneID: aPhoneID   } }).then(function successCallback(response)
                                                                                                                                                            {

                                                                                                                                                                  //    alert("* android  *****Inside the $http post: RESPONSE is: " + JSON.stringify(response));
                                                                                                                                                                      console.log("token is: " + token);

                                                                                                                                                                           var synchedFlag = 0;
                                                                                                                                                                           synchedFlag = parseInt(response.data.synched);


                                                                                                                                                                                console.log("synchedFlag == " + synchedFlag);


                                                                                                                                                                      /*    if (synchedFlag == 1)
                                                                                                                                                                           {



                                                                                                                                                                              var zbprid = parseInt(window.localStorage['bprid']);

                                                                                                                                                                                          function updateBPR() {
                                                                                                                                                                                                    //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                                                                                                                                                                                    uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE rowid = ?";
                                                                                                                                                                                                    //alert("uQuery == " + uQuery);
                                                                                                                                                                                                 //window.sqlitePlugin.execute(db, uQuery, [1, zbprid ]).then(function(res) {

                                                                                                                                                                                            console.log(" Row Updated Successfully! res JSON: " + JSON.stringify(res));


                                                                                                                                                                                                 }, function (err) {
                                                                                                                                                                                                   alert("update/synch error" + JSON.stringify(err));
                                                                                                                                                                                                 });
                                                                                                                                                                                             }

                                                                                                                                                                                            updateBPR();


                                                                                                                                                                                  }
                                                                                                                                                                          */



                                                                                                                                                             }, function errorCallback(response) {
                                                                                                                                                                          // called asynchronously if an error occurs
                                                                                                                                                                          // or server returns response with an error status.


                                                                                                                                                                             //alert("Error: " + response);
                                                                                                                                                                             alert("Error Sending Data to CHG: " + JSON.stringify(response));
                                                                                                                                                                              //console.log(response.data.message);



                                                                                                                                                                        });

                                                                                                                                             } //end postBPData







                                                                                                                                                          window.localStorage['fSys'] = $scope.result.finalSys;
                                                                                                                                                          window.localStorage['fDia'] = $scope.result.finalDia;
                                                                                                                                                          window.localStorage['fHr'] = $scope.result.finalHr;
                                                                                                                                                          window.localStorage['fMap'] = $scope.result.finalMap;

                                                                                                                                                          $scope.result.finalSys = parseInt($scope.result.finalSys);
                                                                                                                                                          $scope.result.finalDia = parseInt($scope.result.finalDia);

                                                                                                                                                    //    alert("#####  sys: " + $scope.result.finalSys + " ; dia: " + $scope.result.finalDia );


                                                                                                                                                             //****ADD SYMPTOMS LATER!
                                                                                                                                                             //check for high BP (MEDICAL Alerts)
                                                                                                                                                             //$state.go('tab.bpStep4');
                                                                                                                                                             //OK SYS: 90-159 // OK DIA: 40-99
                                                                                                                                                             //HIGH SYS: >=160 // HIGH Dia: >=100
                                                                                                                                                             //LOW SYS: <90 //LOW DIA: <40
                                                                                                                                                                if ($scope.result.finalSys != null )
                                                                                                                                                                {
                                                                                                                                                                 //alert("3. BP is ok - NORMAL Result");
                                                                                                                                                                 //OLD values here...
                                                                                                                                                                 // $rootScope.$broadcast('eventFired', finalSys, finalDia, finalHr, finalMap);

                                                                                                                                                                $state.go('tab.btSSResult');

                                                                                                                                                                }
                                                                                                                                                                else {
                                                                                                                                                                  console.log("$scope.result.finalSys is NULL >> NO RESULT");
                                                                                                                                                                }






                                                                                                                                                          /*   if ($scope.result.finalSys > 90 & $scope.result.finalSys < 159 & $scope.result.finalDia > 40 & $scope.result.finalDia < 99 )
                                                                                                                                                             {
                                                                                                                                                              //alert("3. BP is ok - NORMAL Result");
                                                                                                                                                              //OLD values here...
                                                                                                                                                              // $rootScope.$broadcast('eventFired', finalSys, finalDia, finalHr, finalMap);

                                                                                                                                                             $state.go('tab.btSSResult');

                                                                                                                                                             }
                                                                                                                                                             else if ($scope.result.finalSys >=160 || $scope.result.finalDia >= 100)
                                                                                                                                                             {
                                                                                                                                                            //  alert("3. Your BP is HIGH");
                                                                                                                                                              $state.go('tab.bpWarning1');
                                                                                                                                                            }
                                                                                                                                                            else if ($scope.result.finalSys < 90 || $scope.result.finalDia < 40)
                                                                                                                                                            {
                                                                                                                                                            //  alert("3. Your BP is LOW");
                                                                                                                                                              $state.go('tab.bpWarningLow');
                                                                                                                                                            } */

                                                                                                                                                          /*
                                                                                                                                                            else if (symptomCount > 1)
                                                                                                                                                            {
                                                                                                                                                            //  alert("3. You've >1 Pregnancy Symptoms'");
                                                                                                                                                              $state.go('tab.bpWarningLow');

                                                                                                                                                            }
                                                                                                                                                            else if ( symptomCount >= 1 & finalSys >= 140 & finalSys <= 159  ||  finalDia >=90  & finalDia <= 99  )
                                                                                                                                                            {
                                                                                                                                                             //alert("3. Borderline High BP WITH >= 1 Symptom");
                                                                                                                                                             $state.go('tab.bpWarningCombined1');

                                                                                                                                                            }
                                                                                                                                                            */





                                                                                                                                                     } //end  if (byteArrayCount == 3)

















                                                                          }  //end else if (ie no bpm errors [114 - 1,2,3,5 etc] so PARSE etc...)







                                                                                  } //end onRawData()

                                                                                  function subscribeFailed()
                                                                                  {
                                                                                    //alert("subscribe btSerial failed");
                                                                                  }




                                  } //end subscribeBTSerial
































}) //end of btSS (bluetooth SImple Stupid) Controller



.controller('DashCtrl', function($rootScope, encDcrypt, $q, $cordovaProgress, synchAppCHGService, networkService, bleService, testLocalDBService, btSerialService, chgServerApi, $base64, $scope, $http, $state, $timeout, CipherService, Auth, $cordovaSQLite, $ionicModal, $cordovaBluetoothLE) {

//check the user is auth'd properly...
 Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );

   $scope.bpDataSys = window.localStorage['finalSys'];
    $scope.bpDataDia = window.localStorage['finalDia'];
      $scope.bpDataHr =  window.localStorage['finalHr'];
      $scope.bpDataMap = window.localStorage['finalMap'];

$scope.testSQLC2 = function()
{

        /*    db.executeSql("pragma table_info (test_table);", [], function(res)
              {
        alert("app.js: PRAGMA res: " + JSON.stringify(res));
      }); */
          alert("testSQLC2().....");

                                                  db.transaction(function(tx) {

                                                  tx.executeSql("select count(id) as cnt from test_table;", [], function(tx, res) {
                                                    alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                                    alert("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
                                                    });
                                            });

}




$scope.testSQLC = function()
{

//window.sqlitePlugin.echoTest(successCallback, errorCallback);
/*

          db.transaction(function(tx) {
              //alert("slqc.my.db SUCCESS... tx: " + JSON.stringify(tx));





                          //  tx.executeSQL(query, [dateTime, rawDataDate, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmid, phID]).then(function(res) {



                    var query = "INSERT INTO bp_reading_processed (createdAt, rawDataDate, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmID, phoneID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                  //  tx.executeSql("INSERT INTO test_table (data, data_num) VALUES (?,?)", ["test", 100], function(tx, res) {
                    tx.executeSql(query, ["2016-04-05 19:30:23", "2016-04-05 19:30:23", 1601, 0, 139, 80, 99, 140, 81, 100, 141, 82, 101, 140, 81, 100, 99.99, 1, 1 ], function(tx, res) {


                        console.log("Sample Data Inserted into bp_reading_processed table :)");
                         alert("INSERTED bpReadingProcessed. The ID is:" + res.insertId);

                    //  alert("insertId: " + res.insertId + " -- probably 1");
                    //  alert("rowsAffected: " + res.rowsAffected + " -- should be 1");



                              /*  db.transaction(function(tx) {
                                tx.executeSql("select count(id) as cnt from test_table;", [], function(tx, res) {
                                //  alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                //  alert("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
                                  });
                          });*/


/*


                  });



          }, function(err) {
            alert('Open database ERROR: ' + JSON.stringify(err));
          });

*/






            db.transaction(function(tx) {

                     var query = "INSERT INTO bp_reading_processed (createdAt, rawDataDate, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmID, phoneID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                     tx.executeSql(query, ["2016-04-05 19:30:23", "2016-04-05 19:30:23", 1601, 0, 139, 80, 99, 140, 81, 100, 141, 82, 101, 140, 81, 100, 99.99, 1, 1 ], function(tx, res) {


                          alert("Inserted a row Successfully. res.insertId: " + res.insertId);


                                     db.transaction(function(tx) {
                                      tx.executeSql("select * from bp_reading_processed;", [], function(tx, res) {
                                       alert("res.rows.length: " + res.rows.length);
                                       alert("res.rows.item(0).cnt: " + res.rows.item(0).sys1 + " -- should be 139");
                                       alert("all rows (res): " + JSON.stringify(res));

                                     }); //end inner tx >> select *
                                }); //end inner d.TA
                        }); //end outer tx >> insert into

            },
             function(err) {
              alert('app db ERROR: ' + JSON.stringify(err));
            });













}



$scope.captureHeadaches = function(item)
{

$scope.Headaches = item.checked;
console.log("Headaches?: " + $scope.Headaches);


}


$scope.captureVisD = function(item1)
{
$scope.VisD = item1.checked;
console.log("Vis D?: " + $scope.VisD);

}

$scope.captureEpiG = function(item2)
{
$scope.EpiG = item2.checked;
console.log("EpiG?: " + $scope.EpiG);

}

$scope.captureNone = function(item3)
{
$scope.None = item3.checked;
console.log("No Symptoms?: " + $scope.None);

}




  $scope.setResultVars = function(sys, dia, hr, map)
  {

          /*    $scope.bpDataSys = sys;
              $scope.bpDataDia = dia;
              $scope.bpDataHr =  hr;
              $scope.bpDataMap = map; */
              console.log("''''''''''''''''' setResultVars().. sys:" + sys + "dia: " + dia);
                 window.localStorage['finalSys'] = sys;
                  window.localStorage['finalDia']
                    window.localStorage['finalHr']
                    window.localStorage['finalMap']







        //  alert("************** resultVars SET: " + "sys: " + $scope.bpDataSys + "; dia: " + $scope.bpDataDia + "; hr: " + $scope.bpDataHr);

           $timeout(function () {

             $state.go('tab.bpStep4');

  }, 3000);



 }




 //$scope.setResultVars();






  $scope.$on('eventFired', function(event, sys, dia, hr, map) {
        //$scope.someFunction();
        // alert("$emit received!! Setting Vars Now!");
         //$scope.setResultVars();

             //these vars are for the results screen
      /*      $scope.bpDataSys = window.localStorage['finalSys'];
            $scope.bpDataDia = window.localStorage['finalDia'];
            $scope.bpDataHr =  window.localStorage['finalHr'];
            $scope.bpDataMap = window.localStorage['finalMap']; */

              //dont think this will make a difference...

                    //alert has right values BUT the screen show the old values!!
                    //THIS alert fires 4-5 times when using $emit... > Whyyy

              alert("eventFired... Sys: " + sys + "; Dia: " + dia + "; Hr: " + hr);


              $scope.bpDataSys = sys;
              $scope.bpDataDia = dia;
              $scope.bpDataHr =  hr;
              $scope.bpDataMap = map;


              $scope.setResultVars(sys, dia, hr, map);


         //alert("************** resultVars SET: " + "sys: " + $scope.bpDataSys + "; dia: " + $scope.bpDataDia + "; hr: " + $scope.bpDataHr);



    })





 console.log("loggedIn_USERID => " + window.localStorage['loggedIn_USERID']);

var isAndroid = ionic.Platform.isAndroid();
//returns false on ios...
//alert("Dash ctrl... >> isAndroid:: " + isAndroid);

 //var currentPlatform = ionic.Platform.platform();
 //alert(" ionic.Platform.platform() ==>" +  currentPlatform);


          $scope.testPromises= function()
          {

            var defer = $q.defer();

            defer.promise
                .then(function() {
                  alert("I promised I would show up!");
                })
                .then(function() {
                   alert("me too");
                })
                .then(function(){
                  alert("and I!");
                })

              defer.resolve();

          }







                      $scope.initBPReading = function()
                      {



                          if (isAndroid == true)
                          {

                            btSerialService.connect();


                          }
                          else if (isAndroid == false)
                          {
                            //alert("isAndroid == false :) ... doing  test ble plugin call..");
                            bleService.init();


                          $scope.initX  = function()
                                {

                                alert(">>>>>>>>>>>>starting init ble....");

                                              $cordovaBluetoothLE.initialize({request:true}).then(null,
                                              function(obj) {
                                                //Handle errors
                                                  alert("Initialize Error : " + JSON.stringify(obj));

                                              },
                                              function(obj) {
                                                //Handle successes
                                                alert("Initialize Success : " + JSON.stringify(obj));

                                                  //it doesnt seem to call this method...
                                              //  scanBle();
                                              var bpmXid = "5E9B7376-173F-FE68-264F-38169673DFD8";
                                              $scope.connX(bpmXid);





                                              }
                                            );


                                }


                                $scope.connX = function(bpmUUID)
                                {

                                      //alert("starting connect ble....");
                                        $cordovaBluetoothLE.connect({address: bpmUUID }).then(null, function(obj)
                                        {


                                                  //Handle errors
                                              alert("Connect Error! Message: " + JSON.stringify(obj));


                                                                if (obj.message == "Connection failed" )
                                                                {

                                                                    //showBPData();
                                                                  //console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                                                  //  this.disconnect();
                                                                 console.log("***** CLOSING BT CONN");
                                                                    closeConn(bpmUUID);
                                                                  //  console.log("***** FINISHED CLOSING BT CONN");

                                                                }







                                              },
                                              function(obj) {
                                                if (obj.status == "connected")
                                                {

                                                  //This works... :) //subscribe next...
                                              alert("connected. obj is: " + JSON.stringify(obj));

                                                  //may need to do a scan first as it gives a "service not found" error
                                                  //do a discover/ services (ios)... first..
                                               alert("call showServices() ");
                                               $scope.servicesX(bpmUUID);

                                                }
                                                else if (obj.status == "connecting") {
                                                  //Device connecting
                                                  console.log("connecting....");

                                                } else {

                                                    console.log("Conn. else... Should trigger on ios. window.localStorage['isAndroid'] is: " + window.localStorage['isAndroid']);



                                                              console.log("disconnected...");
                                                                    //  console.log(" Calling showBPData().... just after disconnected....");
                                                                      //showBPData();
                                                                  //  console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                                                    //  this.disconnect();

                                                           console.log("***** CLOSING BT CONN");
                                                                //closeConn(bpmUUID);
                                                                console.log("***** FINISHED CLOSING BT CONN");

                                                          }






                                                }
                                              ); //end connect.then...




                                    } //end connX

                                    $scope.servicesX = function(bpmUUID)
                                    {

                                      alert("In servicesX");
                                        var params = {address: bpmUUID , serviceUuids: [ ] };
                                        $cordovaBluetoothLE.services(params).then(success, error);



                                                      function success(obj)
                                                      {
                                                    alert("Services Success : " + JSON.stringify(obj));
                                                      //it gets as far as here...

                                                        if (obj.status == "services")
                                                        {
                                                          console.log("obj.status == Services");

                                                        // var serviceUuids = obj.services;
                                                      //  alert("serviceUuids[1]: " + JSON.stringify(servicesUuids[1]));
                                                      alert("obj.services: " + JSON.stringify(obj.services[2]));

                                                         var fff0Service =  obj.services[2];
                                                        alert("fff0Service: " + fff0Service + " ; " );

                                                          //THIS WORKS!!!!! >> getting CN now on bpm :)!! yay!!!
                                                          // data show appear as an console.log when reading is complete
                                                          //WHY DOES IT GET CHS THO?
                                                          //it actually fails to subscribe with this line...
                                                          //subscribeBle(addr, fff0Service,   "fff1");

                                                          //seems to fall over here...
                                                          //skip it for now
                                                         //addService(addr, scope.fff0Service );
                                                         //console.log("svc not added. skipped this for now.");



                                                          for (var i = 0; i < serviceUuids.length; i++)
                                                          {
                                                            $cordovaBluetoothLE.addService(obj.address, serviceUuids[i]);
                                                            console.log("adding svc["+i+"]. serviceUuid:  " + serviceUuids[i]  );
                                                          }



                                                            //now call subscribe >> Noooo >> need to get characteristics first
                                                            //console.log("Getting characteristics now...");
                                                        //var fff0Service = "fff0";
                                                      //  getCharacteristics(fff0Service);
                                                    //  $scope.getCHx(fff0Service, bpmUUID);


                                                          //subscribeBle(bpmUUID, fff0Service, "fff1");




                                                        }
                                                        else
                                                        {
                                                        alert("Unexpected Services Status");
                                                        }
                                                      }

                                                      function error(obj)
                                                      {
                                                        alert("Services Error : " + JSON.stringify(obj));
                                                      }






                                    } //end servicesX


                                              $scope.getCHx = function(svc, bpmUUID)
                                                {
                                                  alert("++ Starting getCHS... ::" + bpmUUID + " . svc: " + svc);

                                                //  var svcXX = "FFF0";

                                                var params = {address: bpmUUID , service: svc}
                                                $cordovaBluetoothLE.characteristics(params).then(success, error);

                                                                function success(obj)
                                                                {
                                                                 alert("Characteristics Success : " + JSON.stringify(obj));

                                                                  if (obj.status == "characteristics")
                                                                  {
                                                                    alert("obj.status == characteristics");

                                                                    var characteristics = obj.characteristics;

                                                                          var fff1ch = 'FFF1';
                                                                          var fff2ch = 'FFF2';

                                                                          alert("About to subscribeBle... >> " + bpmUUID +  svc + fff1ch );
                                                                        //  subscribeBle(bpmUUID, svc, fff1ch);



                                                                  }
                                                                  else
                                                                  {
                                                                    alert("Unexpected Characteristics Status");
                                                                  }
                                                                }

                                                                function error(obj)
                                                                {
                                                                alert("Characteristics Error : " + JSON.stringify(obj));
                                                                }




                                                } //end getCHs


                                    //call init
                                  //  $scope.initX();












                            }
                            else
                            {
                                alert("Platform is NOT Android or iOS. Only iOS and Android are supported.");
                            }


                          } //end initBPReading()




                          $scope.doSynch = function(uid, flag)
                          {

                            /*
                            uid = window.localStorage['loggedIn_USERID'];
                            flag = 0;

                            var rowsToSynch = null;


                              var query = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
                            //  var query = "SELECT rowid, createdAt, rawDataDate, userID, finalSys, finalDia, finalHr, finalMap, bpmID, phoneID FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
                              $cordovaSQLite.execute(db, query, [uid, flag ]).then(function(res)
                              {
                                  console.log(" res JSON: " + JSON.stringify(res));
                                   rowsToSynch = res.rows.length;
                                   //console.log("4. rowsToSynch: " + rowsToSynch);

                                      if(res.rows.length > 0)
                                        {

                                        }


                                        for (var s = 0; s < rowsToSynch; s++)
                                        {

                                          //DEOSNT WORK... SENDS same row in 4 times ffs
                                          console.log(s + "Calling   synchAppCHGService.synch()");
                                          synchAppCHGService.synch(window.localStorage['loggedIn_USERID'], 0);

                                        }



                                 });



                            //console.log("1. rowsToSynch: " + rowsToSynch); //== null (executes first)
                            //console.log("2. >>> end of function::: synchAppCHGService.synch(window.localStorage['loggedIn_USERID'], 0)");
                            */


                            synchAppCHGService.synch(window.localStorage['loggedIn_USERID'], 0);





                          }

                              $scope.doTestdo = function()
                              {

                                synchAppCHGService.testSynch();

                              }

                              $scope.resetFlags = function()
                              {
                                synchAppCHGService.resetFlags();


                              }







            $scope.cordovaHttp = function()
            {



                              cordovaHTTP.enableSSLPinning(true, function() {
                                        alert('success!');
                                }, function() {
                                alert('error :(');
                                });


                              /*
                                //ALSO REFUSES to work...
                    cordova.exec(
                              useAuth, // A callback function that deals with the JSON object from the CDVPluginResult instance
                              useAuthError, // An error handler
                              'CordovaHTTP', // What class to target messages to (method calls = message in ObjC)
                              'enableSSLPinning', // Which method to call
                              [ 'true' ] // These go in the CDVInvokedUrlCommand instance's.arguments property
                              );

                              */










            }













                                        $scope.closeBTConn = function()
                                        {


                                            //$scope.mlAddress3 = "5E9B7376-173F-FE68-264F-38169673DFD8";
                                              var params = {address: window.localStorage['mlAddress']};
                                            console.log("close: " + JSON.stringify(params));

                                            var iStr1 = "close bt conn: " + JSON.stringify(params);
                                               $scope.addToLog(iStr1, 5);


                                                    $cordovaBluetoothLE.close(params).then(function(obj) {
                                                      console.log("Close Success : " + JSON.stringify(obj));
                                                      }, function(obj) {
                                                        console.log("Close Error : " + JSON.stringify(obj));
                                                      });




                                        }












                         $scope.insertTestData = function(name, year ) {

                        /*    alert("starting insertTestData()");
                                var query = "INSERT INTO test (name, year) VALUES (?,?)";
                                $cordovaSQLite.execute(db, query, [name, year]).then(function(res) {
                                alert("INSERTED test data . The ID is:" + res.insertId);
                                window.localStorage['testDataid'] = res.insertId;
                                 //alert("-- in insert proc. data. method window.localStorage['appDB_bprID'] == " + window.localStorage['bprid'] );


                                    //  alert("calling postBPData() from inside insert to appdb)");
                                    //  $scope.postBPData();
                                    //  alert("DONE calling postBPData()");



                                }, function (err) {
                                  alert(err);
                                });   */

                                testLocalDBService.insertTestData(name, year);



                            }




                              $scope.updateTestData = function(idd, year) {

                                //do test insert first...
                                //(db, "CREATE TABLE IF NOT EXISTS test (idd integar primary key, name text, year text)")

                            /*    alert("starting updateTestData()...");
                                        //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                        uQuery = "UPDATE test SET year=? WHERE rowid=1";
                                        alert("uQuery == " + uQuery);
                                     $cordovaSQLite.execute(db, uQuery, [year]).then(function(res) {

                                     alert(" Row Updated Successfully! res JSON: " + JSON.stringify(res));


                                     }, function (err) {
                                       alert("update/synch error" + JSON.stringify(err));
                                     });
                                     */


                                     testLocalDBService.updateTestData(idd, year);




                                 }





                                 $scope.readTestData = function(idd)
                                 {
                                   //alert(1);

                                /*     var query = "SELECT rowid, * FROM test";
                                       $cordovaSQLite.execute(db, query).then(function(res) {
                                           if(res.rows.length > 0) {
                                             //  alert("SELECTED row0 -> " + res.rows.item(0).createdAt + " - " + res.rows.item(0).base64Data + " - " + res.rows.item(0).phoneID);
                                               alert("Now, here's all the data");


                                                               for(var i = 0; i < res.rows.length; i++) {


                                                                // alert("bpID: " +  res.rows.item(i).rowid + "; CreatedAt Time: " + res.rows.item(i).createdAt + "; base64Data: " + res.rows.item(i).base64Data + "; hexData: " + res.rows.item(i).hexData + "; Outcome: " + res.rows.item(i).readingOutcome
                                                                //    + "; Outcome Details: " + res.rows.item(i).readingOutcomeDetails );

                                                                    alert( "TEST DATA Row: id: " + res.rows.item(i).rowid + "; Name: " + res.rows.item(i).name + "; Year: " + res.rows.item(i).year );


                                                                  }

                                                            }



                                 });  */


                                 testLocalDBService.readTestData(idd);




                               }




                    $scope.testSynch = function()
                    {

                      var arrx = [];
                      arrx  = SynchAppCHG.checkAppDB(1601);
                      //alert("arrx => " + arrx);
                      alert("  $scope.itemsToBeSynched => " + JSON.stringify( arrx ));


                    }










                      $scope.updateBPR_test = function(value, bprID) {

                        //do test insert first...
                        //(db, "CREATE TABLE IF NOT EXISTS test (idd integar primary key, name text, year text)")





                        alert("starting updateBPR_test()...");
                                //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE bppID = ?";
                                alert("uQuery == " + uQuery);
                             window.sqlitePlugin.execute(db, uQuery, [value, bprID]).then(function(res) {

                             alert(" Row Updated Successfully! res JSON: " + JSON.stringify(res));


                             }, function (err) {
                               alert("update/synch error" + JSON.stringify(err));
                             });
                         }













                      //   $scope.updateBPR();





                      //this fucntion reads in ALL of the b64 strings from the app db, converts them to byteArrays and prints out each array element >> which gives us the bp data
                      $scope.readSQLiteBPData = function(uid) {

                        //still no results found
                        /* alert("uid is: " + uid);
                        alert("userIDx: " + uid);
                          var userIDx = parseInt(uid);
                          */


                            //  var query = "SELECT rowid, * FROM bp_reading_raw WHERE userID  = ?";
                            var query = "SELECT rowid, * FROM bp_reading_raw WHERE userID  = ?";
                            window.sqlitePlugin.execute(db, query, [uid]).then(function(res) {
                                  if(res.rows.length > 0) {
                                    //  alert("SELECTED row0 -> " + res.rows.item(0).createdAt + " - " + res.rows.item(0).base64Data + " - " + res.rows.item(0).phoneID);
                                      alert("Now, here are all the readings for userID: " + uid);


                                                      for(var i = 0; i < res.rows.length; i++) {


                                                        alert("bpID: " +  res.rows.item(i).rowid + "; CreatedAt Time: " + res.rows.item(i).createdAt + "; base64Data: " + res.rows.item(i).base64Data + "; hexData: " + res.rows.item(i).hexData + "; Outcome: " + res.rows.item(i).readingOutcome
                                                           + "; Outcome Details: " + res.rows.item(i).readingOutcomeDetails );


                                                            if(window.localStorage['isAndroid'] == "false")
                                                            {
                                                                        //only for ble... ie ios
                                                                        var ib64 = res.rows.item(i).base64Data;
                                                                            //convert each b64Value into a byteArray and print each element of each array


                                                                            var bytesZ = $cordovaBluetoothLE.encodedStringToBytes(ib64);
                                                                            console.log("******Starting byteArray.....");
                                                                            for (var j = 0; j < bytesZ.length; j++)
                                                                            {
                                                                              console.log("bytesZ[" + j + "] is: " + bytesZ[j]);

                                                                            }
                                                                            console.log("******Ending byteArray.....");

                                                              }
                                                              else {
                                                              //  alert("not ble");
                                                              }



                                                      } // end for loop






                                  }
                                  else {
                                    alert("No results found");
                                  }
                              }, function (err) {
                                  alert(err);
                              });
                          } //end readSQLiteBPData()




                           $scope.readSQLiteBPProcessedData = function(uid) {

                             //alert("start of read processed bp data...");
                                   //var query = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ?";
                                   var query = "SELECT rowid, * FROM bp_reading_processed";
                                   //var query = "SELECT * FROM bp_reading_processed";
                                   window.sqlitePlugin.execute(db, query, [uid]).then(function(res) {
                                   //$cordovaSQLite.execute(db, query).then(function(res) {
                                       if(res.rows.length > 0) {
                                    //  alert("#ROW 0 #: Time: " + res.rows.item(0).createdAt + "; bppID " + res.rows.item(0).rowid + "; PhoneID: " + res.rows.item(0).phoneID + "; Avgs::: Sys: " + res.rows.item(0).finalSys);
                                      alert("Now, here are all the processed BP readings : " + JSON.stringify(res));

                                        //alert(JSON.stringify(res));

                                                           for(var i = 0; i < res.rows.length; i++) {


                                                            alert("bpID: " +  res.rows.item(i).rowid + "; CreatedAt Time: " + res.rows.item(i).createdAt + "; finalSys: " + res.rows.item(i).finalSys + "; finalDia: " + res.rows.item(i).finalDia  + "; finalHr: " + res.rows.item(i).finalHr
                                                               + "; finalMap: " + res.rows.item(i).finalMap + "; chgSynched (flag): " + res.rows.item(i).chgSynched );






                                                           }




                                       } else {
                                          alert("No results found");
                                       }
                                   }, function (err) {
                                    alert(JSON.strinfigy(err));
                                   });
                               } //end readSQLiteBPProcessedData







                     $scope.disconnect = function()
                     {

                          var params = {address: window.localStorage['mlAddress']};

                        console.log("Attempting Disconnect to: " + params);

                            $cordovaBluetoothLE.disconnect(params).then(function(obj) {
                          console.log("Disconnect Success : " + JSON.stringify(obj));
                            }, function(obj) {
                            console.log("Disconnect Error : " + JSON.stringify(obj));
                            });





                     }





                        //RD's ng Read Version
                        //call this from a button... once subscribed
                        //$scope.subscribeBle($scope.mlAddress, $scope.fff0Service,  $scope.fff1ch);
                        //calls
                      //  $scope.readRD($scope.mlAddress, $scope.fff0Service,  $scope.fff1ch);
                      //  $scope.readRD($scope.mlAddress, $scope.fff0Service,  $scope.fff2ch);

                        $scope.readRD = function(address, service, characteristic) {
                          var params = {address:address, service:service, characteristic:characteristic, timeout: 2000};

                          console.log("Read : " + JSON.stringify(params));

                          $cordovaBluetoothLE.read(params).then(function(obj) {
                            console.log("Read Success : " + JSON.stringify(obj));

                            var bytes = $cordovaBluetoothLE.encodedStringToBytes(obj.value);
                            //console.log("Read : " + bytes[0]);
                          }, function(obj) {
                            console.log("Read Error : " + JSON.stringify(obj));
                          });
                        };



                        //create the handler functions for the buttons - to call the readRD()

                        $scope.readfff0 = function()
                        {
                              $scope.readRD($scope.mlAddress, $scope.fff0Service,  $scope.fff1ch);
                        }


                        $scope.readfff1 = function()
                        {
                              $scope.readRD($scope.mlAddress, $scope.fff0Service,  $scope.fff1ch);
                        }









                    $scope.writeBle = function()
                    {

                        var string1 = "MÿQ";
                        var bytes =   $cordovaBluetoothLE.stringToBytes(string1);
                        var encodedString1 =   $cordovaBluetoothLE.bytesToEncodedString(bytes);

                        var newHexString = b64toHex(encodedString1);
                        console.log("New Hex String is: " + newHexString);
                        //console.log("New Hex String is: " + newHexString);

                        var params = {address:$scope.mlAddress, serviceUuid:$scope.fff0Service, characteristicUuid: $scope.fff1ch , value: newHexString};
                        console.log("Writing to : " + JSON.stringify(params));
                        $cordovaBluetoothLE.write(params).then(success, error);



                              function success(obj)
                              {

                                          console.log("Write Success!!!! WOW!!! - obj.value: " + JSON.stringify(obj.value));




                              }

                              function error(obj)
                              {

                                  console.log("write error :(" +  JSON.stringify(obj))


                              }


                    } //end write()







                          function hexStringToByteArrayNew(s) {
                            var len = s.length;
                            var data = [];

                           //WHAT is this line for??
                          //  for(var i=0; i<(len/2); data[i++]=0) {}

                            for (var i = 0; i < len; i += 2) {

                                var value = (parseInt(s.charAt(i), 16) << 4)  + parseInt(s.charAt(i+1), 16);

                                // "transforms" your integer to the value a Java "byte" would have:
                                if(value > 127) {
                                    value -= 256;
                                }

                                data[i / 2] = value;

                            }

                            return data;

                        };








                          $scope.parseHex = function()
                          {

                            var parsedHex = hexStringToByteArrayNew("4d516b003b000077433b3a7589a36f");
                            console.log("parsedHex is: " + parsedHex);

                          }








                    //my read function version
                  $scope.readBle = function(addr)
                    {
                  //trying to read now...
                  console.log("doing read now");
                  var params = {address: addr, serviceUuid: "fff0", characteristicUuid: "fff1"};
                  $cordovaBluetoothLE.read(params).then(success, error);

                            function success(obj)
                            {

                                          console.log("Read Success : " + JSON.stringify(obj));

                                          if (obj.status == "read")
                                          {
                                            /*var bytes = bluetoothle.encodedStringToBytes(obj.value);
                                            console.log("Read : " + bytes[0]);*/

                                            console.log("obj.status == Read");
                                          }
                                          else
                                          {
                                            console.log("Unexpected Read Status");
                                          }


                            }


                            //@1740 ON JAN21: KEEP getting read error: no connection address present
                            //@1743: got rid of ""s around the filed names
                            //@1748: Still get error... >> getCharacteristics works tho so keep trying that ... tack
                            function error(obj)
                            {
                              console.log("Read Error : " + JSON.stringify(obj));

                            }




                }  //end readBle()













// $scope.rating = 5;
//APP rating
 $scope.rating = {};
$scope.rating.numberSelection = 6;

$scope.rating.othernumberSelection = "";




//THe rest of DashCtrl etc from here...............

                    var userEncPassword = "stephen";



                         $scope.signOut = function()
                         {
                           console.log("&&&&&&&& ********* Signing out...");
                           window.localStorage['TOKEN'] = null;
                           window.localStorage['loggedIn_USERID'] = null;
                            window.localStorage['loggedIn_USERNAME'] = null;


                              $scope.user = null;
                              $state.go('signin');

                         }


                        console.log('Welcome to Home / Dashboard');
                        console.log("Token: " + window.localStorage['TOKEN'] );
                        console.log("UserID: " + window.localStorage['loggedIn_USERID'] );


                                  var token = window.localStorage['TOKEN'];

                                  /*
                                      // do a test call to the db, with the token....
                                    //$http({  method: 'POST', url: 'http://leanbh.stephenlane.me:8080/api/allUsers', data: { token: token } }).then(function successCallback(response)
                                    $http({  method: 'POST', url: chgServerApi.serverName + '/api/allUsers', data: { token: token } }).then(function successCallback(response)
                                    {
                                                  // this callback will be called asynchronously
                                                  // when the response is available

                                                    console.log(response);
                                                    console.log(response.data.message);
                                                    console.log("Auth: " + response.data.auth);



                                                    $scope.allUsers = response.data;


                                     }, function errorCallback(response) {
                                                  // called asynchronously if an error occurs
                                                  // or server returns response with an error status.


                                                      console.log(response);
                                                      //console.log(response.data.message);
                                                      console.log("Auth: " + response.data.auth);



                                                });

                                                */

                                                /*

                                //enc testing...
                                var text = "stephenlane921@gmail.com";

                                var eText = {};

                                eText = CipherService.encrypt(text, userEncPassword );

                                console.log("Encrypted Text is:::  " + JSON.stringify(eText));
                                //console.log("Cipher Text: " + eText.cipher_text);
                                console.log("salt: " + eText.salt );
                                console.log("iv: " + eText.iv);


                                //decrypt the cipher text now...
                                //decrypt function Format:: (cipherText, password, salt, iv)
                                var dText = {}
                                dText = CipherService.decrypt(eText.cipher_text, userEncPassword, eText.salt, eText.iv);
                                console.log("Decrypted Text is:::"  + dText);

                                console.log("----------------------");

                                var ctxt  = "FY2K+E9lS1FNTklpT0JzUTIrVmFDZmp3dnc9PQ==";
                                var iv_ = "QHzxFp3iM+34EkQY";

                                var salt_ = "";



                                var dText2 = {};
                                 //dText2 = CipherService.decrypt(ctxt, userEncPassword, salt_, iv_);
                                //console.log("dText2 is: " + dText2);




                                console.log("----------------------");


                                //DOESNT DISPLAY ANYTHING...
                                //this is what chg CryptoJS does:  (b64, hex, hex) {"ct":"xp75UcUmR70KD5oGMVmimA==","iv":"2f568a7c28b6ec82a0bc3c407cace11a","s":"519d806bf9406b19"}
                                //b64::: iv: L1aKfCi27IKgvDxAfKzhGg== ; salt: UZ2Aa/lAaxk= ;
                                    var dpass = "stephen";
                                    var div = "L1aKfCi27IKgvDxAfKzhGg==";
                                    var dsalt = "UZ2Aa/lAaxk=";
                                    var dcipherText = "xp75UcUmR70KD5oGMVmimA==";

                                    //dTextn = CipherService.decrypt(eText.cipher_text, userEncPassword, eText.salt, eText.iv);
                                    //dTextn = CipherService.decrypt(dcipherText, dpass, dsalt, div);

                                  //  console.log("dTextn: " + dTextn);







                                //var iv = "1234567891234567";

                                var test1 = encDcrypt.enc("test@test.com");
                                console.log("enc'd test1: " + test1);

                            //  var st = "xp75UcUmR70KD5oGMVmimA==";


                                var dTest1 = encDcrypt.decrypt(test1);
                                console.log("decrypt'd test1: " + dTest1);
                                console.log("----------------------");

                                var manual_data = 'Pw5ZDSYX3HAHuJNQvVkECQ==';

                                var crypttext = "xp75UcUmR70KD5oGMVmimA==";
                                var iv5 = CryptoJS.enc.Hex.parse('2f568a7c28b6ec82a0bc3c407cace11a');
                                var salt2 = CryptoJS.enc.Hex.parse('519d806bf9406b19');


                                var decrypted = CryptoJS.AES.decrypt(
                                  crypttext,
                                  dpass,
                                  {
                                    iv: iv5,
                                    mode: CryptoJS.mode.CBC,
                                    padding: CryptoJS.pad.Pkcs7
                                  }
                                );
                                console.log('^^^^^^decrypted, by hand: '+decrypted.toString(CryptoJS.enc.Utf8));

                                */



                                //NEED THE
                              //  CipherService.ed();

                                //var cParams = {"ct":"xp75UcUmR70KD5oGMVmimA==","iv":"2f568a7c28b6ec82a0bc3c407cace11a","salt":"519d806bf9406b19"};
                              //  var c2p = {};
                              //  c2p = encDcrypt.stringifyParams(cParams);
                            //    console.log("c2p: " + c2p);




                                /*

                                  console.log("----------------------");

                                            var C = CryptoJS;

                                              //var message = C.enc.Hex.parse('00112233445566778899aabbccddeeff');
                                              var key = "stephen";
                                              var message = "super secret code";
                                              var salt = C.enc.Hex.parse('519d806bf9406b19');
                                              var iv = C.enc.Hex.parse('2f568a7c28b6ec82a0bc3c407cace11a');
                                              console.log("iv:: " + iv);

                                              var expectedMessage = message.toString();
                                              //console.log(expectedMessage);
                                              var expectedKey = key.toString();
                                              var expectedIv = iv.toString();

                                              var etxt11 = C.AES.encrypt(message, key, { iv: iv, salt: salt });
                                              console.log("etxt11: " + etxt11);


                                              var etxt12 = C.enc.Hex.parse('C69EF951C52647BD0A0F9A063159A298');
                                              var dtxt11 = C.AES.decrypt(etxt11, key, { iv: iv, salt: salt });
                                              console.log("dtxt11: " + dtxt11);

                                    console.log("----------------------");


                                          //b64 to hex:     xp75UcUmR70KD5oGMVmimA== =>> C69EF951C52647BD0A0F9A063159A298
                                          var msg3 = "C69EF951C52647BD0A0F9A063159A298";
                                          var iv2 = "2f568a7c28b6ec82a0bc3c407cace11a";
                                          var key2 = "519d806bf9406b19";

                                          var dtxt2 = C.AES.decrypt(msg3, key2, {iv: iv2 });
                                          console.log("dtxt2: " + dtxt2);

                                          /*
                                          $scope.decrypt11 = function()
                                          {


                                             C.AES.decrypt(C.lib.CipherParams.create({ ciphertext: C.enc.Hex.parse('C69EF951C52647BD0A0F9A063159A298') }),
                                             C.enc.Hex.parse('000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f'),
                                            { mode: C.mode.ECB, padding: C.pad.NoPadding }).toString();


                                          }
                                          */
                                          //trying more CJ...HERE

                                        //  var x = {"ct":"xp75UcUmR70KD5oGMVmimA==","iv":"2f568a7c28b6ec82a0bc3c407cace11a","s":"519d806bf9406b19"};
                                        //  var spassword = "stephen";


                                        //  var plaintextArray = CryptoJS.AES.decrypt( { ciphertext: CryptoJS.enc.Latin1.parse(crypttext) }, passw, { iv: CryptoJS.enc.Latin1.parse(iv2), salt: salt2 });
                                          //console.log("CJ decode... :" + CryptoJS.enc.Latin1.stringify(plaintextArray));
                                            /*console.log("----------------------");
                                          console.log(x.ct.toString(CryptoJS.enc.Utf8));
                                          console.log(x.iv.toString(CryptoJS.enc.Utf8));
                                          console.log(x.s.toString(CryptoJS.enc.Utf8));
                                          */



                                        //  console.log(JSON.stringify(x));
                                          //console.log(x.ct.toString(CryptoJS.enc.Utf8));
                                            /*
                                             parse: function (jsonStr)
                                             {
                                              var j = JSON.parse(jsonStr);
                                              var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
                                              if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
                                              if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
                                              return cipherParams;
                                            }
                                            */


                                        //
                                        /*
                                          //var encryptedX = CryptoJS.AES.encrypt(JSON.stringify("y@y.com"), "my passphrase", {iv: CryptoJS.enc.Hex.parse(x.iv), s: CryptoJS.enc.Hex.parse(x.s)} ).toString();
                                          var encryptedX = CryptoJS.AES.encrypt("y@y.com", "11111");
                                          console.log("encryptedX: " + encryptedX);
                                          var dx0 = CryptoJS.AES.decrypt(encryptedX, "11111");

                                            var spassword = "stephen";
                                        //  var dx = CryptoJS.AES.decrypt(x.ct.toString(CryptoJS.enc.Utf8), spassword, {iv: CryptoJS.enc.Hex.parse(x.iv), s: CryptoJS.enc.Hex.parse(x.s) } );
                                          //console.log(JSON.stringify(dx.toString(CryptoJS.enc.Utf8))); >> works when I encrypt something myself!!!
                                          console.log("dx0: " + dx0.toString(CryptoJS.enc.Utf8));
                                        //  console.log("dx: " + CryptoJS.enc.base64.parse(dx.toString(CryptoJS.enc.Utf8)));
                                          //REFUSES to work ^^^^^!
                                          */
                                  /*       console.log("----------------------");
                                          console.log("----------------------");
                                          console.log("----------------------");

                                          var CryptoJSAesJson =
                                          {
                                              stringify: function (cipherParams) {
                                                  var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
                                                  if (cipherParams.iv) j.iv = cipherParams.iv.toString();
                                                  if (cipherParams.salt) j.s = cipherParams.salt.toString();
                                                  return JSON.stringify(j);
                                              },
                                              parse: function (jsonStr) {
                                                  var j = JSON.parse(jsonStr);
                                                  var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(JSON.stringify(j.ct))});
                                                      //console.log(cipherParams);
                                                  if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
                                                  if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
                                                      console.log(cipherParams);
                                                  return cipherParams;
                                              }
                                          }

                                          //  var x = '{"ct":"xp75UcUmR70KD5oGMVmimA==","iv":"2f568a7c28b6ec82a0bc3c407cace11a","s":"519d806bf9406b19"}';
                                      //  var x =  '{"ct":"fREGHAq4BCIvSI2VIrs+kR\/2LSYQQQlQ2xBzh9wiEKE=","iv":"3e584d5e6e32bbe075ac65fa38fdb5cf","s":"c90f59001da8e37f"}';
                                        var x = '{"ct":"yT0FUNM8UToNV8N1Rk7/CA5Yi+Y3dvNcjpK7LF2ChLY=","iv":"c2c564588f3cb964deb13c7eeb6334d5","s":"df32765a8909e4a9"}';

                                          //  console.log(JSON.stringify(x));

                                            var spassword = "password";

                                          //var encrypted = CryptoJS.AES.encrypt(JSON.stringify("mcirl2@gmail.com"), spassword, {format: CryptoJSAesJson}).toString();

                                        //  console.log("ecd::: " + encrypted);
                                        //console.log(x);
                                          //var decrypted = JSON.parse(CryptoJS.AES.decrypt(encrypted, spassword, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
                                          //    console.log("decrypted=> " + decrypted);
                                          var decrypted = JSON.parse(CryptoJS.AES.decrypt(x, spassword, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
                                                      //                CryptoJS.AES.decrypt($(".encrypted").val(), $(document.e).find(".pass").val(), {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8)));
                                          console.log("decrypted=> " + decrypted);



                                          */







                                                                    /*      //enc testing...
                                                                          var text = "stephenlane921@gmail.com";

                                                                          var eText = {};

                                                                          eText = CipherService.encrypt(text, userEncPassword );

                                                                          console.log("Encrypted Text is:::  " + JSON.stringify(eText));
                                                                          //console.log("Cipher Text: " + eText.cipher_text);
                                                                          console.log("salt: " + eText.salt );
                                                                          console.log("iv: " + eText.iv);


                                                                          //decrypt the cipher text now...
                                                                          //decrypt function Format:: (cipherText, password, salt, iv)
                                                                          var dText = {}

                                                                          var ict = "fREGHAq4BCIvSI2VIrs+kR\/2LSYQQQlQ2xBzh9wiEKE=";
                                                                          var iiv = "";

                                                                        //  dText = CipherService.decrypt(eText.cipher_text, userEncPassword, eText.salt, eText.iv);
                                                                          console.log("Decrypted Text is:::"  + dText);

                                                                          console.log("----------------------");

                                                                          var ctxt  = "FY2K+E9lS1FNTklpT0JzUTIrVmFDZmp3dnc9PQ==";
                                                                          var iv_ = "QHzxFp3iM+34EkQY";

                                                                          var salt_ = "3e584d5e6e32bbe075ac65fa38fdb5cf";



                                                                          var dText2 = {};
                                                                           //dText2 = CipherService.decrypt(ctxt, userEncPassword, salt_, iv_);
                                                                          //console.log("dText2 is: " + dText2);




                                                                          console.log("----------------------");
                                                                          */







                                        $scope.testEnc = function()
                                        {

                                                //do login api call...
                                                //alert("server::: " + chgServerApi.serverName);
                                                $http({  method: 'GET', url: 'http://www.mikecunneen.com/enc.php?data=abc' }).then(function successCallback(response)
                                                {


                                                console.log("calling enc.php " + JSON.stringify(response));









                                                 }, function errorCallback(response) {
                                                              // called asynchronously if an error occurs
                                                              // or server returns response with an error status.

                                                             alert("enc.php Error. Please Check your Internet Connection");
                                                              //  alert(JSON.stringify(response));

                                                                  //console.log(response.data.message);




                                                            }); //end $http post









                                        }






















$scope.showUser = function()
{
alert("user: " + window.localStorage["loggedIn_USERID"] + "; -- token: " + window.localStorage["TOKEN"]);

}



}) //end dash ctrl







.controller('SignInCtrl', function($scope, $state, $http, chgServerApi, networkService, Auth) {
  //window.localStorage["loggedIn_USERID"] = null;
  //window.localStorage['TOKEN'] = null;

   Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );

 $scope.user = {};

//test for now...




                     $scope.signIn = function(user) {




                                  if (user.username == null || user.password == null || user.username == "" || user.password == "")
                                  {

                                  alert("You MUST enter a USERNAME and PASSWORD");

                                  }
                                  else
                                  {

                                  //alert("about to log in... >> server::: " + chgServerApi.serverName);


                                             var sUsername = user.username.toLowerCase();
                                             var sPassword = user.password;


                                             //reset the form values
                                             //user.username = null;
                                            // user.password = null;


                                          //  var netConn = networkService.check();
                                            netConn = 1; //for web app testing
                                            if (netConn == 1)
                                            {

                                            //  alert("Internet Connection Present. Logging you in!");



                                                        //do login api call...
                                                        //alert("server::: " + chgServerApi.serverName);
                                                        $http({  method: 'POST', url: chgServerApi.serverName + '/api/login', data: { username: sUsername, password: sPassword } }).then(function successCallback(response)
                                                        {


                                                      //  console.log("*******in http SIGN IN... , resp: " + JSON.stringify(response));


                                                                      if (user.password == response.data.dbPassword)
                                                                      {
                                                                          window.localStorage['TOKEN'] = response.data.token;
                                                                          window.localStorage["loggedIn_USERID"] = response.data.UserID;
                                                                          alert("logged in... response.data.UserID: " + response.data.UserID);
                                                                          window.localStorage['loggedIn_USERNAME'] = response.data.Username;

                                                                          console.log("^^^^^Logged In. Token => " + window.localStorage['TOKEN']);
                                                                          console.log("^^^^^Logged In. userID (chgUserID) => " + window.localStorage["loggedIn_USERID"]);
                                                                          console.log("^^^^^Logged In. username (chg username) => " + window.localStorage["loggedIn_USERNAME"]);

                                                                                          //get imei number for Android
                                                                                          /*if (ionic.Platform.isAndroid() == true)
                                                                                          {

                                                                                                    window.plugins.imeiplugin.getImei(callback);

                                                                                                    function callback(imei)
                                                                                                    {
                                                                                                    //  alert("My Android IMEI: " + imei);
                                                                                                    window.localStorage['IMEI'] = imei;
                                                                                                    }

                                                                                        }*/

                                                                                  window.localStorage['phoneUUID'] = device.uuid;
                                                                                //  alert("device.uuid (phone) => " + window.localStorage['phoneUUID'] );


                                                                                //now we have userid and imei... > store in userdata tbl... asap..
                                                                                var query = "INSERT INTO userData (pin, userID, phoneUUID, bpmID, loginDate, token) VALUES (?,?,?,?,?,?)";

                                                                                var iPin = 1234;
                                                                                var iUserID = window.localStorage["loggedIn_USERID"];
                                                                                var phoneUUID = window.localStorage['phoneUUID'];
                                                                                var iBPMID = "ML00000000000";
                                                                                var dt = new Date();
                                                                                var iLoginDate = dt.toUTCString();
                                                                                var iToken = window.localStorage['TOKEN'];



                                                                                            db.transaction(function(tx) {
                                                                                            tx.executeSql(query, [iPin, iUserID, phoneUUID, iBPMID, iLoginDate, iToken ], function(tx, res) {
                                                                                            // alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                                                                            // alert("insertId: " + res.insertId + " -- probably 1");
                                                                                             //alert("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
                                                                                              });
                                                                                      });




                                                                          $state.go('tab.dash');



                                                                      }
                                                                      else if (response.data.loginFailed == true)
                                                                      {

                                                                          alert("Invalid Username or Password");



                                                                      }
                                                                      else {

                                                                        alert("Unable to Log In. Please try again.");

                                                                      }










                                                         }, function errorCallback(response) {
                                                                      // called asynchronously if an error occurs
                                                                      // or server returns response with an error status.

                                                                     alert("Login Error. Please Check your Internet Connection");
                                                                      //  alert(JSON.stringify(response));

                                                                          //console.log(response.data.message);




                                                                    }); //end $http post


                                                  } //end if network conn == 1
                                                  else {
                                                    alert("Your Phone has not Internet Connection so you can't log in. Please check your Internet Connection and try again.");
                                                  }



                                  } //emd else




                     }





})



.controller('BPCtrl', function($scope, $state, $http, chgServerApi, BloodPressure) {




















})//end bpController


//Log / Health Record
.controller('LogCtrl', function($scope, Auth) {

//Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );


$scope.AllLogData = [];
var logData = [];

//$scope.AllLogData = logDataService.getData(1601);
//alert(JSON.stringify($scope.AllLogData));
//var logData = [];

          //fills up the $scope.logData array
          $scope.getLogData = function(uid)
          {




                            //New Style sqlite - with C >> to replace the other one...
                            db.transaction(function(tx) {


                                  //  alert("My Readings: slqc.my.db SUCCESS... tx: " + JSON.stringify(tx));

                                    var query = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ?";
                                    //tx.executeSql(query, [1601], function(tx, res)
                                  //  var query = "SELECT * FROM bp_reading_processed;";
                                    tx.executeSql(query, [uid], function(tx, res)
                                    {
                                    //  alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                    //  alert("res.rows.item(13).finalSys: " + res.rows.item(0).finalSys);

                                                            if(res.rows.length > 0)
                                                            {

                                                                //alert("Now, here's all the data");


                                                                                        for(var i = 0; i < res.rows.length; i++)
                                                                                        {


                                                                                        /*  logData.id = res.rows.item(i).rowid;
                                                                                          logData.createdAt = res.rows.item(i).createdAt;
                                                                                          logData.readingType = "Blood Pressure";
                                                                                          logData.systolic = res.rows.item(i).finalSys;
                                                                                          logData.diastolic = res.rows.item(i).finalDia;
                                                                                          logData.hr = res.rows.item(i).finalHr;
                                                                                          logData.map = res.rows.item(i).finalMap; */



                                                                                          //var logItem = res.rows.item(i).createdAt + " - Blood Pressure - " + "<br />" + "<b>Systolic:</b>: " + res.rows.item(i).finalSys + " - <b>Diastolic: 0</b>" + res.rows.item(i).finalDia + " - <b>HR:</b> " + res.rows.item(i).finalHr + " - MAP: " + res.rows.item(i).finalMap;
                                                                                        //  alert("res: " + JSON.stringify(res));
                                                                                            logData[i] = res.rows.item(i);
                                                                                           //logData.push(res.rows.item(i));



                                                                                         //logData.push({id: res.rows.item(i).rowid, createdAt: res.rows.item(i).createdAt, readingType: "Blood Pressure", systolic: res.rows.item(i).finalSys, diastolic: res.rows.item(i).finalDia, hr: res.rows.item(i).finalHr, map: res.rows.item(i).finalMap });

                                                                                        } //end loop




                                                                      //  alert("end res.rows.length > 0");

                                                                } //end if res.rows.length >0







                                    }); //end tx.executeSql

                            }, //IF there's a   db.transaction error
                            function(err) {
                              console.log('Log database ERROR: ' + JSON.stringify(err));
                            });






                  //  alert("end of getLogData(). logData[] is: " + JSON.stringify(logData));

            } //end $scope.getLogData()








            $scope.doRefresh = function() {

              $scope.getLogData(window.localStorage['loggedIn_USERID']);
              $scope.readings = logData;


              //ends the refresh
              $scope.$broadcast('scroll.refreshComplete');



                }








        //load log when ctrller loads
          $scope.doRefresh();










}) //end log (chats) ctrl

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})
/*
.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };


}) */



.controller('AccountCtrl', function($scope, Auth)
{

  Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );
//console.log(window.localStorage['loggedIn_USERID']);
$scope.accountID = window.localStorage['loggedIn_USERNAME'];
//alert("$scope.accountType: " + $scope.accountType);

$scope.userData = [];



          db.transaction(function(tx) {
          tx.executeSql("select rowid, * from userData where userID = ? ;", [ window.localStorage['loggedIn_USERID'] ], function(tx, res) {
           //alert("res.rows.length: " + res.rows.length + " -- should be 1");
           //alert("res.rows.item(0).userID: " + res.rows.item(0).userID + "  ; pin " + res.rows.item(0).pin + " ;loginDate " + res.rows.item(0).loginDate + " ; token" + res.rows.item(0).token + " ; phoneUUID" + res.rows.item(0).phoneUUID);
          $scope.userData.push(res.rows.item(0));
            });
          });


   $scope.showUserData = function()
   {

     alert("userData: " + JSON.stringify($scope.userData));
     alert("$scope.userData[0].phoneUUID: " + $scope.userData[0].phoneUUID);


   }




})


.controller('HelpCtrl', function($scope, Auth)
{

  Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );

//console.log(window.localStorage['loggedIn_USERID']);


//$scope.accountID = window.localStorage['loggedIn_USERID'];
//alert("$scope.accountType: " + $scope.accountType);

//


})



.controller('btSerialTestCtrl', function($base64, $scope, $cordovaBluetoothSerial, $cordovaBLE, $cordovaBluetoothLE) {



/*
//$scope.mlMacAddress = "8C:DE:52:1F:B6:12";
$scope.mlMacAddress = null;
$scope.readData = "Inital Read Data - ie nothing from device yet!";

//===========BLE CENTRAL=============



        $scope.showBTSettings = function()
        {
        alert("Show BT Settings + enable");

          ble.showBluetoothSettings();

          ble.enable(success, failure);


        }


          $scope.blecScan = function()
          {


                      function failure(reason) {
                              alert("BLE Scan Failed for: " + reason);
                          }


            ble.scan([], 10, function(device) {
                      alert("In SCAN method");


                          alert("Device(s) are: " + JSON.stringify(device));
                          $scope.mlMacAddress = device.id;

                          //  $scope.blecConnect();

                            }, failure);






        }



  $scope.blecBPData2 = [];
  //$scope.blecBPData2[0] = 222;
  //  $scope.blecBPData2[1] = 111;

$scope.blecBPData = [];





        //seems to work...
        $scope.blecConnect = function()
        {
              //1st is mac address and second is
              $scope.mlMacAddress = "8C:DE:52:1F:B6:12";

            //uuid in droid gives conn failure. mac addrr gets stuck in connecting....
            //$scope.mlMacAddress = "5E9B7376-173F-FE68-264F-38169673DFD8";

          alert("Starting blec connect to: " + $scope.mlMacAddress);





                ble.connect($scope.mlMacAddress, connectSuccess, connectFailure);


              function connectSuccess()
                {
                  alert("Connect Success! Now Subscribing.....");

                    var onData = function(buffer)
                    {
                      // Decode the ArrayBuffer into a typed Array based on the data you expect
                      var data = new Uint8Array(buffer);
                      alert("Data Available!! data[0]: " + data[0] + "data.length => " + data.length);

                                for (var t=0; t < data.length; t++)
                                {
                                //  alert("data["+t+"]: " + data[t]);
                                  $scope.blecBPData.push(data[t]);




                                }



                    }
                    //IT fucking works!!!!
                  ble.startNotification($scope.mlMacAddress, "FFF0", "FFF1", onData, failure);

                            function failure()
                            {

                              alert("Subscribe/Notification  FAIL");


                            }








                }

                function connectFailure()
                {
                alert("Connect Failure. ie End of rdg");

                    alert("BP Data: " + $scope.blecBPData);

                    $scope.blecBPData2 = $scope.blecBPData;




                }



        } //end connect









        $scope.blecWrite = function()
        {

                    console.log("about to write ack cmd to fff0.fff2!");
                    //CMD: 4D FF 02 03 51 => Receive Success, Clear Mem.

                    var data = new Uint8Array(5);
                    data[0] = 0x4D;
                    data[1] = 0xFF;
                    data[2] = 0x02;
                    data[3] = 0x03;
                    data[4] = 0x51;
                    //ble.write(device_id, "fff0", "fff1", data.buffer, success, failure);

                        function success()
                        {
                          //keep getting this but dont believe it's actually working...
                          //but better than failure!
                          console.log("Write Success. Data is " + JSON.stringify(data));

                        }

                        function failure()
                        {
                            //keep getting this
                            console.log("Write Failure. Data is" + JSON.stringify(data));

                        }

                  ble.write($scope.mlMacAddress, "FFF0", "FFF2", data, success, failure);





        }




          $scope.blecRead = function()
          {

              alert("Doing Read to FFF0 > FFF1");

                function success(data)
                {
                  console.log("Read Success. Data is " + JSON.stringify(data));

                }

                function failure(data)
                {
                    //keep getting this > data is: Read failed >> try after reading
                    console.log("Read Failure. Data is" + JSON.stringify(data));

                }

            ble.read($scope.mlMacAddress, "FFF0", "FFF1", success, failure);





          }





//=======end BLEC =========

*/





  /*


            //=====START OF BTSERIAL CODE.... for ANDROID only!!


            //array of all the bp data from BPM (via serialConn and NOT BLE)
                $scope.btSerialBPData = [];

                    $scope.enableBT = function()
                    {
                      alert("About to Turn on BT");

                                bluetoothSerial.enable(
                                function() {
                                  alert("Bluetooth is enabled");
                                },
                                function() {
                                    alert("The user did *not* enable Bluetooth");
                                }
                            );
                    }



                    $scope.connectBTSerial = function()
                    {
                      //eventually set this to the var from chg... (btSerial uses mac while ble uses uuid)
                      //var addrx = window.localStorage['bpmMacAddress'];
                      var addrx = "8C:DE:52:1F:B6:12";

                      bluetoothSerial.connect(addrx, connectSuccess, connectFailure);


                                function connectSuccess()
                                {
                                alert("Connected! Calling subcribe()");
                                $scope.subscribeBTSerial();
                                }

                                function connectFailure()
                                {
                                  alert("Connection Failure!");

                                      if ( $scope.btSerialBPData.length > 0)
                                      {
                                          alert("btSerial BP Data Array is: " + JSON.stringify(  $scope.btSerialBPData ));

                                      }
                                      else
                                      {
                                          alert("Connection Failure + No Data from BPM");
                                      }


                                }

                    } //end connectBTSerial()




                    $scope.subscribeBTSerial = function()
                    {

                          alert("about to subscribe (raw data).....");

                            bluetoothSerial.subscribeRawData(onRawData, subscribeFailed);

                                function onRawData(data)
                                {
                                    window.alert("raw data present!!");

                                      var rawData = new Uint8Array(data);
                                        window.alert("rawData (json) is: " + JSON.stringify(rawData) );

                                        //this fucking works :) :) :)
                                        for (var e = 0; e < rawData.length; e++)
                                        {
                                        //alert("rawData["+e+"]: " + rawData[e]);
                                        $scope.btSerialBPData.push(rawData[e]);
                                        }


                                }

                                function subscribeFailed()
                                {
                                  alert("subscribe failed");
                                }


                    } //end subscribeBTSerial()
                    //=====//END OF BTSERIAL CODE.... for ANDROID only!!




                      //for testing only... it fills the array that then appears on the test screen... IGNORE
                        $scope.showSubRawData = function()
                        {
                        //  alert("Sub.Raw.Data is: " +   $scope.subRawData  );
                        $scope.btSerialBPData2 = $scope.btSerialBPData;
                        }




                      //can be called when BPM reading is done.
                      $scope.bytesAvailable = function()
                      {
                                      bluetoothSerial.available(function (numBytes) {
                                      alert("There are " + numBytes + " available to read.");
                                    });

                      }

                        //Cant get this working .... BUT Write to fff0.fff2 AND put \n or \r (almost sure its \n) at end of call/msg
                        $scope.writeBTSerial = function()
                        {

                          console.log("about to write ack cmd!");
                          //CMD: 4D FF 02 03 51 => Receive Success, Clear Mem.

                        var data = new Uint8Array(5);
                        data[0] = 0x4D;
                        data[1] = 0xFF;
                        data[2] = 0x02;
                        data[3] = 0x03;
                        data[4] = 0x51;
                        bluetoothSerial.write(data, success, failure);

                        }


                        $scope.subRawData = null;





            //99% sure I dont need below method
              /*
                $scope.hex2bytes = function(s) {
                  var len = s.length;
                  var data = [];

                 //WHAT is this line for??
                //  for(var i=0; i<(len/2); data[i++]=0) {}

                  for (var i = 0; i < len; i += 2) {

                      var value = (parseInt(s.charAt(i), 16) << 4)  + parseInt(s.charAt(i+1), 16);

                      // "transforms" your integer to the value a Java "byte" would have:
                      if(value > 127) {
                          value -= 256;
                      }

                      data[i / 2] = value;

                  }

                  return data;

              };








              $scope.bpArrayX = [];

            //not very useful for raw data coming from bpm... the method returns a string... IGNORE IT
            $scope.readBTSerial = function()
            {



              bluetoothSerial.read(function (data) {

                  if (data != null)
                  {

                    //data now should be an arraY...
                  alert("data[] is: " + JSON.stringify(data));
                  //  alert("data11 = " + data[11]);
                  //  alert("data12 = " + data[12]);
                  //  alert("data13 = " + data[13]);

                    window.localStorage["ReadData"] = data;
                    alert("wlsReadData is " + window.localStorage["ReadData"]);


                  //  var newBytes = $scope.hex2bytes(data);
                  //  alert("newBytes[]: " + newBytes);
                  //  alert(newBytes.length);



                    $scope.readData = data;

                                for (var x = 0; x < data.length; x++)
                                {
                                //  alert("data[" + x+  "] is: " + data[x]);

                                  $scope.bpArrayX.push(data[x]);

                                }

                  }

                  //im 90% sure that data is a b64 string... >> so need to convert to byteArray
            //    alert("Read Data is: " + data + "----- Now doing decode...");


                 $scope.decodeB64(data);


                //var decodedData = $scope.decodeB64(data);
                //alert("decodedData = " + decodedData);
                //  alert("decodedData[0]-[1]... = " + decodedData[0] + " &&&  " + decodedData[1] );



              }) //end read()


            } //end $scope.readBTSerial()








            /*


            //=================================== b64 Stuff
        //doesnt work with the data from read -- as its a mangled string...
        $scope.decodeB64 = function()
        {

          //serial plugin read method gives us string. Need to encode this into b64...

          //data = window.localStorage["ReadData"];
          alert("***Decoding this data (wls): " +  window.localStorage["ReadData"]);



            var encodedStringX = $base64.encode( window.localStorage["ReadData"] );
             alert("encodedStringX = " + encodedStringX);

              var bytes = $cordovaBluetoothLE.encodedStringToBytes(encodedStringX);
              alert("bytes.length: " + bytes.length );


	       }






            //==================================



            $scope.btSerialBPData2 = null;







              //dont need this either I think...
            $scope.showBPDataX = function()
            {
              alert($scope.bpArrayX);
              $scope.bpArrayX2 = $scope.bpArrayX;

              alert( "wlsRead Data is: " +   window.localStorage["ReadData"]);

                    var bytesZ = new Uint8Array(window.localStorage["ReadData"]);

                  alert("bytesZ.length => " + bytesZ.length );
                  alert("bytesZ[0]: " + bytesZ[0]  + "bytesZ[1]: " + bytesZ[1] + "bytesZ[2]: " + bytesZ[2]);


            }


            */



}) // end bt serial ctrl
